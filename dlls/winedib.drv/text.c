/*
 * DIBDRV text functions
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/* copies bitmapped font to DIB */
extern COLORREF DIBDRV_SetPixel( DIBDRVPHYSDEV *physDev, INT x, INT y, COLORREF color );
static void DrawCharBitmap(DIBDRVPHYSDEV *physDev, INT x, INT y, FT_Bitmap *bmp)
{
    physDev->bmp.funcs->freetype_blit(&physDev->bmp, x, y, bmp);
}

/***********************************************************************
 *           DIBDRV_ExtTextOut
 */
BOOL
DIBDRV_ExtTextOut( DIBDRVPHYSDEV *physDev, INT x, INT y, UINT flags,
                   const RECT *lprect, LPCWSTR wstr, UINT count,
                   const INT *lpDx )
{
    /* FIXME : TODO many, many stuffs... just trivial text support by now */

    FT_Face face;
    FT_UInt glyph_index;
    INT n;
    INT error;
    LOGFONTW lf;
    LPCWSTR wstrPnt;
    
    FT_Glyph glyph;
    FT_BitmapGlyph bitmap;
    double cosEsc, sinEsc;
    FT_Matrix matrix;
    FT_Vector start; 
    int dx, dy;
    
    TRACE("physDev:%p, x:%d, y:%d, flags:%x, lprect:%p, wstr:%s, count:%d, lpDx:%p\n",
        physDev, x, y, flags, lprect, debugstr_w(wstr), count, lpDx);

    face = physDev->face;
    if(!face)
    {
        ERR("FreeType face is null\n");
        return TRUE;
    }
    
    /* gets font data, etc */
    GetObjectW(GetCurrentObject(physDev->hdc, OBJ_FONT), sizeof(lf), &lf);

    /* sets character pixel size
      FIXME : strange behaviour, don't know why the 64 and 20 numbers... */
    error = pFT_Set_Char_Size(
        face,                                      /* handle to face object */
        MulDiv(abs(lf.lfWidth), 64, 20),           /* char_width in 1/64th of points */
        MulDiv(abs(lf.lfHeight), 64, 20),          /* char_height in 1/64th of points */
        DIBDRV_GetDeviceCaps(physDev, LOGPIXELSX), /* horizontal device resolution */
        DIBDRV_GetDeviceCaps(physDev, LOGPIXELSY)  /* vertical device resolution */ 
    );
    if(error)
        ERR("Couldn't set char size to (%d,%d)\n", lf.lfWidth, lf.lfHeight);

    /* transformation matrix and vector */
    start.x = 0;
    start.y = 0;
    if(lf.lfEscapement != 0)
    {
        cosEsc = cos(lf.lfEscapement * M_PI / 1800);
        sinEsc = sin(lf.lfEscapement * M_PI / 1800);
    }
    else
    {
        cosEsc = 1;
        sinEsc = 0;
    }
    matrix.xx = (FT_Fixed)( cosEsc * 0x10000L );
    matrix.xy = (FT_Fixed)(-sinEsc * 0x10000L );
    matrix.yx = (FT_Fixed)( sinEsc * 0x10000L );
    matrix.yy = (FT_Fixed)( cosEsc * 0x10000L );
 
    /* outputs characters one by one */
    wstrPnt = wstr;
    for ( n = 0; n < count; n++ )
    {
        /* retrieve glyph index from character code */
        if(flags & ETO_GLYPH_INDEX)
            glyph_index = *wstrPnt++;
        else
            glyph_index = pFT_Get_Char_Index( face, *wstrPnt++);

        /* load glyph image into the slot (erase previous one) */
        error = pFT_Load_Glyph( face, glyph_index, FT_LOAD_DEFAULT );
        if(error)
        {
            ERR("Couldn't load glyph at index %d\n", glyph_index);
            /* ignore errors */
            continue;
        }
        error = pFT_Get_Glyph(face->glyph, &glyph);
        if ( error )
        {
            FIXME("Couldn't get glyph\n");
            continue; 
        }
 
        /* apply transformation to glyph */
        if ( glyph->format != FT_GLYPH_FORMAT_BITMAP )
            pFT_Glyph_Transform(glyph, &matrix, &start ); 
                
        /* gets advance BEFORE transforming... */
        dx = glyph->advance.x;
        dy = glyph->advance.y;
         
        /* convert to an anti-aliased bitmap, if needed */
        if ( glyph->format != FT_GLYPH_FORMAT_BITMAP )
        {
            error = pFT_Glyph_To_Bitmap(
                &glyph,
                FT_RENDER_MODE_NORMAL,
                0, /* no additional translation */
                1  /* destroy copy in "image" */
            );  

            /* ignore errors */
            if ( error )
            {
                FIXME("Couldn't render glyph\n");
                pFT_Done_Glyph(glyph);
                continue;
            }
        }

        /* now, draw to our target surface */
        bitmap = (FT_BitmapGlyph)glyph;
        DrawCharBitmap(physDev, x+bitmap->left, y-bitmap->top, &bitmap->bitmap);

        /* increment pen position */
        x += dx>>16;
        y -= dy>>16;

        pFT_Done_Glyph(glyph);
    } 

    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetTextExtentExPoint
 */
BOOL DIBDRV_GetTextExtentExPoint( DIBDRVPHYSDEV *physDev, LPCWSTR str, INT count,
                                  INT maxExt, LPINT lpnFit, LPINT alpDx, LPSIZE size )
{
    TRACE("physDev:%p, str:%s, count:%d, maxExt:%d, lpnFit:%p, alpDx:%p, size:%p\n",
        physDev, debugstr_w(str), count, maxExt, lpnFit, alpDx, size);

#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}
