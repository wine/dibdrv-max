/*
 * DIBDRV font objects
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/**********************************************************************
 *          DIBDRV_SetTextColor
 */
COLORREF DIBDRV_SetTextColor( DIBDRVPHYSDEV *physDev, COLORREF color )
{
    COLORREF oldColor;
    INT r, g, b;
    INT i;
    
    TRACE("physDev:%p, color:%x\n", physDev, color);

    /* do nothing if color is the same as actual one */
    if(color == physDev->textColor)
    return color;

    /* stores old color */
    oldColor = physDev->textColor;
    
    /* fills the text color table used on antialiased font display */
    if(physDev->bmp.funcs)
    {
        r = GetRValue(color);
        g = GetGValue(color);
        b = GetBValue(color);
        for(i = 0; i < 256; i++)
        {
            physDev->bmp.textColorTable[i] = physDev->bmp.funcs->colorref_to_pixel(&physDev->bmp, RGB(
                MulDiv(r, i, 256),
                MulDiv(g, i, 256),
                MulDiv(b, i, 256)
            ));
            
        }
    }
    
    /* returns previous text color */
    return oldColor;
}

/***********************************************************************
 *           DIBDRV_SelectFont
 */
HFONT DIBDRV_SelectFont( DIBDRVPHYSDEV *physDev, HFONT hfont, GdiFont *gdiFont )
{
    FT_Int      i;
    FT_Error    error;
    FT_CharMap  charmap = NULL;
    LOGFONTW lf;
    GdiFont *curFont;
    
    FIXME("physDev:%p, hfont:%p, gdiFont:%p\n", physDev, hfont, gdiFont);

    /* gets font information */
    GetObjectW(hfont, sizeof(lf), &lf);
    TRACE("Font is : '%s'\n", debugstr_w(lf.lfFaceName));
    
    /* FIXME: just handles gdifont, don't know if it needs to handle hfont too
       BTW, still don't know how to get FT_Face from non-gdi font here
    */
    if(!gdiFont)
    {
        FIXME("No gdi font - unhandled by now.\n");
        return hfont;
    }

    physDev->face = gdiFont->ft_face;
    if(!physDev->face)
    {
        FIXME("Error, null Ft_Face\n");
        return hfont;
    }

    /* setup the correct charmap.... maybe */
    for (i = 0; i < physDev->face->num_charmaps; ++i)
    {
        if (physDev->face->charmaps[i]->platform_id != TT_PLATFORM_MICROSOFT)
        continue;

        if (physDev->face->charmaps[i]->encoding_id == TT_MS_ID_UNICODE_CS)
        {
            charmap = physDev->face->charmaps[i];
            break;
        }

        if (charmap == NULL)
        {
            WARN("Selected fallout charmap #%d\n", i);
            charmap = physDev->face->charmaps[i];
        }
    }
    if (charmap == NULL)
    {
        WARN("No Windows character map found\n");
        charmap = physDev->face->charmaps[0];
        return FALSE;
    }

    error = pFT_Set_Charmap(physDev->face, charmap);
    if (error != FT_Err_Ok)
    {
        ERR("%s returned %i\n", "FT_Set_Charmap", error);
        return FALSE;
    }

    /* we use GDI fonts, so just return false */
    return 0;
}

/***********************************************************************
 *           DIBDRV_EnumDeviceFonts
 */
BOOL DIBDRV_EnumDeviceFonts( DIBDRVPHYSDEV *physDev, LPLOGFONTW plf,
                             FONTENUMPROCW proc, LPARAM lp )
{
    TRACE("physDev:%p, plf:%p, proc:%p, lp:%lx\n", physDev, plf, proc, lp);

    /* don't enumerate x11 fonts, we're using client side fonts */
    return FALSE;
}

/***********************************************************************
 *           DIBDRV_GetTextMetrics
 */
BOOL DIBDRV_GetTextMetrics( DIBDRVPHYSDEV *physDev, TEXTMETRICW *metrics )
{
    TRACE("physDev:%p, metrics:%p\n", physDev, metrics);

    /* no need for this one, as we use GDI fonts */
    return FALSE;
}

/***********************************************************************
 *           DIBDRV_GetCharWidth
 */
BOOL DIBDRV_GetCharWidth( DIBDRVPHYSDEV *physDev, UINT firstChar, UINT lastChar,
                          LPINT buffer )
{
    TRACE("physDev:%p, firstChar:%d, lastChar:%d, buffer:%pn", physDev, firstChar, lastChar, buffer);

    /* no need for this one, as we use GDI fonts */
    return FALSE;
}
