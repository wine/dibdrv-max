/*
 * DIBDRV pen objects
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Huw Davies
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

static const dash_pattern_t dash_patterns[4] =
{
    {2, {18, 6}},
    {2, {3,  3}},
    {4, {9, 6, 3, 6}},
    {6, {9, 3, 3, 3, 3, 3}}
};

static inline void order_end_points(int *s, int *e)
{
    if(*s > *e)
    {
        int tmp;
        tmp = *s + 1;
        *s = *e + 1;
        *e = tmp;
    }
}

static void solid_pen_hline(DIBDRVPHYSDEV *physDev, INT start, INT end, INT row)
{
    order_end_points(&start, &end);
    physDev->bmp.funcs->solid_hline(&physDev->bmp, start, end, row, physDev->pen_and, physDev->pen_xor);
}

static void solid_pen_vline(DIBDRVPHYSDEV *physDev, INT col, INT start, INT end)
{
    order_end_points(&start, &end);
    physDev->bmp.funcs->solid_vline(&physDev->bmp, col, start, end, physDev->pen_and, physDev->pen_xor);
}


static void WINAPI solid_pen_line_callback(INT x, INT y, LPARAM lparam)
{
    DIBDRVPHYSDEV *physDev = (DIBDRVPHYSDEV *)lparam;

    physDev->bmp.funcs->set_pixel(&physDev->bmp, x, y, physDev->pen_and, physDev->pen_xor);
    return;
}

void solid_pen_line(DIBDRVPHYSDEV *physDev, INT x1, INT y1, INT x2, INT y2)
{
    LineDDA(x1, y1, x2, y2, solid_pen_line_callback, (LPARAM)physDev);
}

static inline void get_dash_colors(DIBDRVPHYSDEV *physDev, DWORD *and, DWORD *xor)
{
    if(physDev->mark_space == mark)
    {
        *and = physDev->pen_and;
        *xor = physDev->pen_xor;
    }
    else if(GetBkMode(physDev->hdc) == OPAQUE)
    {
        *and = physDev->bkgnd_and;
        *xor = physDev->bkgnd_xor;
    }
    else
    {
        *and = 0xffffffff;
        *xor = 0;
    }
}

static inline void next_dash(DIBDRVPHYSDEV *physDev)
{
    if(physDev->left_in_dash != 0) return;

    physDev->cur_dash++;
    if(physDev->cur_dash == physDev->pen_pattern->count) physDev->cur_dash = 0;
    physDev->left_in_dash = physDev->pen_pattern->dashes[physDev->cur_dash];
    if(physDev->mark_space == mark) physDev->mark_space = space;
    else physDev->mark_space = mark;
}

static void dashed_pen_hline(DIBDRVPHYSDEV *physDev, INT start, INT end, INT row)
{
    INT cur_x = start;
    DWORD and, xor;
    DWORD dash_len;

    if(start <= end)
    {
        while(cur_x != end)
        {
            get_dash_colors(physDev, &and, &xor);

            dash_len = physDev->left_in_dash;
            if(cur_x + dash_len > end)
                dash_len = end - cur_x;

            physDev->bmp.funcs->solid_hline(&physDev->bmp, cur_x, cur_x + dash_len, row, and, xor);
            cur_x += dash_len;

            physDev->left_in_dash -= dash_len;
            next_dash(physDev);
        }
    }
    else
    {
        while(cur_x != end)
        {
            get_dash_colors(physDev, &and, &xor);

            dash_len = physDev->left_in_dash;
            if(cur_x - (INT)dash_len < end)
                dash_len = cur_x - end;

            physDev->bmp.funcs->solid_hline(&physDev->bmp, cur_x - dash_len + 1, cur_x + 1, row, and, xor);
            cur_x -= dash_len;

            physDev->left_in_dash -= dash_len;
            next_dash(physDev);
        }
    }
}

static void dashed_pen_vline(DIBDRVPHYSDEV *physDev, INT col, INT start, INT end)
{
    INT cur_y = start;
    DWORD and, xor;
    DWORD dash_len;

    if(start <= end)
    {
        while(cur_y != end)
        {
            get_dash_colors(physDev, &and, &xor);

            dash_len = physDev->left_in_dash;
            if(cur_y + dash_len > end)
                dash_len = end - cur_y;

            physDev->bmp.funcs->solid_vline(&physDev->bmp, col, cur_y, cur_y + dash_len, and, xor);
            cur_y += dash_len;

            physDev->left_in_dash -= dash_len;
            next_dash(physDev);
        }
    }
    else
    {
        while(cur_y != end)
        {
            get_dash_colors(physDev, &and, &xor);

            dash_len = physDev->left_in_dash;
            if(cur_y - (INT)dash_len < end)
                dash_len = cur_y - end;

            physDev->bmp.funcs->solid_vline(&physDev->bmp, col, cur_y - dash_len + 1, cur_y + 1, and, xor);
            cur_y -= dash_len;

            physDev->left_in_dash -= dash_len;
            next_dash(physDev);
        }
    }
}

static void WINAPI dashed_pen_line_callback(INT x, INT y, LPARAM lparam)
{
    DIBDRVPHYSDEV *physDev = (DIBDRVPHYSDEV *)lparam;
    DWORD and, xor;

    get_dash_colors(physDev, &and, &xor);

    physDev->bmp.funcs->set_pixel(&physDev->bmp, x, y, and, xor);

    physDev->left_in_dash--;
    next_dash(physDev);

    return;
}

static void dashed_pen_line(DIBDRVPHYSDEV *physDev, INT x1, INT y1, INT x2, INT y2)
{
    LineDDA(x1, y1, x2, y2, dashed_pen_line_callback, (LPARAM)physDev);
}

void _DIBDRV_reset_dash_origin(DIBDRVPHYSDEV *physDev)
{
    physDev->cur_dash = 0;
    if(physDev->pen_pattern)
        physDev->left_in_dash = physDev->pen_pattern->dashes[0];
    physDev->mark_space = mark;
}


/* For 1bpp bitmaps, unless the selected foreground color exactly
   matches one of the colors in the colortable, then the color that
   isn't the bkgnd color is used. */
static DWORD adjust_fg_color(DIBDRVPHYSDEV *physDev, COLORREF color)
{
    RGBQUAD rgb;
    int i;

    rgb.rgbRed   = GetRValue(color);
    rgb.rgbGreen = GetGValue(color);
    rgb.rgbBlue  = GetBValue(color);

    for(i = 0; i < physDev->bmp.color_table_size; i++)
    {
        RGBQUAD *cur = physDev->bmp.color_table + i;
        if((rgb.rgbRed == cur->rgbRed) && (rgb.rgbGreen == cur->rgbGreen) && (rgb.rgbBlue == cur->rgbBlue))
            return i;
    }
    return ~physDev->bkgnd_color & 1;
}

static void fixup_fg_colors_1(DIBDRVPHYSDEV *physDev)
{
    INT rop = GetROP2(physDev->hdc);

    physDev->pen_color   = adjust_fg_color(physDev, physDev->pen_colorref);
    physDev->brush_color = adjust_fg_color(physDev, physDev->brush_colorref);

    _DIBDRV_calc_and_xor_masks(rop, physDev->pen_color, &physDev->pen_and, &physDev->pen_xor);
    _DIBDRV_calc_and_xor_masks(rop, physDev->brush_color, &physDev->brush_and, &physDev->brush_xor);
    HeapFree(GetProcessHeap(), 0, physDev->brush_and_bits);
    HeapFree(GetProcessHeap(), 0, physDev->brush_xor_bits);
    physDev->brush_and_bits = NULL;
    physDev->brush_xor_bits = NULL;
}

static void solid_brush_hline(DIBDRVPHYSDEV *physDev, INT start, INT end, INT row)
{
    order_end_points(&start, &end);
    physDev->bmp.funcs->solid_hline(&physDev->bmp, start, end, row, physDev->brush_and, physDev->brush_xor);
}


static void generate_masks(DIBDRVPHYSDEV *physDev, DIBDRVBITMAP *bmp, void **and, void **xor)
{
    INT rop = GetROP2(physDev->hdc);
    DWORD *color_ptr, *and_ptr, *xor_ptr;
    DWORD size = bmp->height * abs(bmp->stride);

    *and = HeapAlloc(GetProcessHeap(), 0, size);
    *xor = HeapAlloc(GetProcessHeap(), 0, size);

    color_ptr = bmp->bits;
    and_ptr = *and;
    xor_ptr = *xor;

    while(size)
    {
        _DIBDRV_calc_and_xor_masks(rop, *color_ptr++, and_ptr++, xor_ptr++);
        size -= 4;
    }
}

static void pattern_brush_hline(DIBDRVPHYSDEV *physDev, INT start, INT end, INT row)
{
    DWORD *and, *xor, brush_row = row % physDev->brush_dib.height;

    if(!physDev->brush_and_bits)
        generate_masks(physDev, &physDev->brush_dib,
                       &physDev->brush_and_bits, &physDev->brush_xor_bits);

    order_end_points(&start, &end);
    and = (DWORD *)((char *)physDev->brush_and_bits + brush_row * physDev->brush_dib.stride);
    xor = (DWORD *)((char *)physDev->brush_xor_bits + brush_row * physDev->brush_dib.stride);

    physDev->bmp.funcs->pattern_hline(&physDev->bmp, start, end, row, and, xor, physDev->brush_dib.width, start % physDev->brush_dib.width);
}

/***********************************************************************
 *           DIBDRV_SelectPen
 */
HPEN DIBDRV_SelectPen( DIBDRVPHYSDEV *physDev, HPEN hpen )
{
    LOGPEN logpen;
    
    TRACE("physDev:%p, hpen:%p\n", physDev, hpen);

    GetObjectW(hpen, sizeof(logpen), &logpen);

    physDev->pen_colorref = logpen.lopnColor;

    if(physDev->bmp.bit_count == 1)
        fixup_fg_colors_1(physDev);
    else
        physDev->pen_color = physDev->bmp.funcs->colorref_to_pixel(&physDev->bmp, logpen.lopnColor);

    _DIBDRV_calc_and_xor_masks(GetROP2(physDev->hdc), physDev->pen_color, &physDev->pen_and, &physDev->pen_xor);

    switch(logpen.lopnStyle)
    {
    default:
        FIXME("Unhandled pen style %d\n", logpen.lopnStyle);
        /* fall through */
    case PS_SOLID:
        physDev->pen_hline = solid_pen_hline;
        physDev->pen_vline = solid_pen_vline;
        physDev->pen_line  = solid_pen_line;
        physDev->pen_pattern = NULL;
        break;

    case PS_DASH:
    case PS_DOT:
    case PS_DASHDOT:
    case PS_DASHDOTDOT:
        physDev->pen_hline = dashed_pen_hline;
        physDev->pen_vline = dashed_pen_vline;
        physDev->pen_line  = dashed_pen_line;
        physDev->pen_pattern = &dash_patterns[logpen.lopnStyle - PS_DASH];
        _DIBDRV_reset_dash_origin(physDev);
        break;
    }

    return hpen;

}

/***********************************************************************
 *           DIBDRV_SetDCPenColor
 */
COLORREF DIBDRV_SetDCPenColor( DIBDRVPHYSDEV *physDev, COLORREF crColor )
{
    TRACE("physDev:%p, crColor:%x\n", physDev, crColor);

#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return crColor;
}

/***********************************************************************
 *           DIBDRV_SelectBrush
 */
HBRUSH DIBDRV_SelectBrush( DIBDRVPHYSDEV *physDev, HBRUSH hbrush )
{
    LOGBRUSH logbrush;

    TRACE("physDev:%p, hbrush:%p\n", physDev, hbrush);

    GetObjectW(hbrush, sizeof(logbrush), &logbrush);

    HeapFree(GetProcessHeap(), 0, physDev->brush_dib.bits);
    HeapFree(GetProcessHeap(), 0, physDev->brush_dib.color_table);
    HeapFree(GetProcessHeap(), 0, physDev->brush_and_bits);
    HeapFree(GetProcessHeap(), 0, physDev->brush_xor_bits);
    physDev->brush_dib.bits = NULL;
    physDev->brush_dib.color_table = NULL;
    physDev->brush_and_bits = NULL;
    physDev->brush_xor_bits = NULL;

    switch (logbrush.lbStyle)
    {
    default:
        FIXME("Unhandled brush style %d\n", logbrush.lbStyle);
        /* fall through */
    case BS_SOLID:
        physDev->brush_hline = solid_brush_hline;
        physDev->brush_colorref = logbrush.lbColor;
        if(physDev->bmp.bit_count == 1)
            fixup_fg_colors_1(physDev);
        else
            physDev->brush_color = physDev->bmp.funcs->colorref_to_pixel(&physDev->bmp, logbrush.lbColor);

        _DIBDRV_calc_and_xor_masks(GetROP2(physDev->hdc), physDev->brush_color,
                                   &physDev->brush_and, &physDev->brush_xor);
        break;

    case BS_DIBPATTERN:
    {
        DIBDRVBITMAP src;
        BITMAPINFO *bmi = GlobalLock16(logbrush.lbHatch);

        _DIBDRV_init_dib_from_BITMAPINFO(&src, bmi);
        _DIBDRV_copy_dib_color_info(&physDev->bmp, &physDev->brush_dib);
        _DIBDRV_convert_dib(&src, &physDev->brush_dib);

        GlobalUnlock16(logbrush.lbHatch);
        physDev->brush_hline = pattern_brush_hline;
        break;
    }

    }
    return hbrush;
}

/***********************************************************************
 *           DIBDRV_SetDCBrushColor
 */
COLORREF DIBDRV_SetDCBrushColor( DIBDRVPHYSDEV *physDev, COLORREF crColor )
{
    TRACE("physDev:%p, crColor:%x\n", physDev, crColor);

#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return crColor;
}

/***********************************************************************
 *           SetROP2
 */
INT DIBDRV_SetROP2( DIBDRVPHYSDEV *physDev, INT rop )
{
    INT ret;
    
    TRACE("physDev:%p, rop:%x\n", physDev, rop);

    ret = GetROP2(physDev->hdc);

    if(ret != rop)
    {
        _DIBDRV_calc_and_xor_masks(rop, physDev->pen_color,   &physDev->pen_and,   &physDev->pen_xor);
        _DIBDRV_calc_and_xor_masks(rop, physDev->brush_color, &physDev->brush_and, &physDev->brush_xor);
        _DIBDRV_calc_and_xor_masks(rop, physDev->bkgnd_color, &physDev->bkgnd_and, &physDev->bkgnd_xor);
        HeapFree(GetProcessHeap(), 0, physDev->brush_and_bits);
        HeapFree(GetProcessHeap(), 0, physDev->brush_xor_bits);
        physDev->brush_and_bits = NULL;
        physDev->brush_xor_bits = NULL;
    }
    return ret;
}

/***********************************************************************
 *           SetBkColor
 */
COLORREF DIBDRV_SetBkColor( DIBDRVPHYSDEV *physDev, COLORREF color )
{
    INT rop;
    
    TRACE("physDev:%p, color:%x\n", physDev, color);

    rop = GetROP2(physDev->hdc);

    physDev->bkgnd_color = physDev->bmp.funcs->colorref_to_pixel(&physDev->bmp, color);

    if(physDev->bmp.bit_count == 1)
        fixup_fg_colors_1(physDev);

    _DIBDRV_calc_and_xor_masks(rop, physDev->bkgnd_color, &physDev->bkgnd_and, &physDev->bkgnd_xor);

    return color;
}
