/*
 * DIB Engine Primitives
 *
 * Copyright 2008 Huw Davies
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/* ------------------------------------------------------------*/
/*                     BASIC ROP HELPER                        */
/*
 *
 * Decompose the 16 ROP2s into an expression of the form
 *
 * D = (D & A) ^ X
 *
 * Where A and X depend only on P (and so can be precomputed).
 *
 *                                       A    X
 *
 * R2_BLACK         0                    0    0
 * R2_NOTMERGEPEN   ~(D | P)            ~P   ~P
 * R2_MASKNOTPEN    ~P & D              ~P    0
 * R2_NOTCOPYPEN    ~P                   0   ~P
 * R2_MASKPENNOT    P & ~D               P    P
 * R2_NOT           ~D                   1    1
 * R2_XORPEN        P ^ D                1    P
 * R2_NOTMASKPEN    ~(P & D)             P    1
 * R2_MASKPEN       P & D                P    0
 * R2_NOTXORPEN     ~(P ^ D)             1   ~P
 * R2_NOP           D                    1    0
 * R2_MERGENOTPEN   ~P | D               P   ~P
 * R2_COPYPEN       P                    0    P
 * R2_MERGEPENNOT   P | ~D              ~P    1
 * R2_MERGEPEN      P | D               ~P    P
 * R2_WHITE         1                    0    1
 *
 */

/* A = (P & A1) | (~P & A2) */
#define ZERO {0, 0}
#define ONE {0xffffffff, 0xffffffff}
#define P {0xffffffff, 0}
#define NOT_P {0, 0xffffffff}

static const DWORD rop2_and_array[16][2] =
{
    ZERO, NOT_P, NOT_P, ZERO,
    P,    ONE,   ONE,   P,
    P,    ONE,   ONE,   P,
    ZERO, NOT_P, NOT_P, ZERO
};

/* X = (P & X1) | (~P & X2) */
static const DWORD rop2_xor_array[16][2] =
{
    ZERO, NOT_P, ZERO, NOT_P,
    P,    ONE,   P,    ONE,
    ZERO, NOT_P, ZERO, NOT_P,
    P,    ONE,   P,    ONE
};

#undef NOT_P
#undef P
#undef ONE
#undef ZERO

void _DIBDRV_calc_and_xor_masks(INT rop, DWORD color, DWORD *and, DWORD *xor)
{
    /* NB The ROP2 codes start at one and the arrays are zero-based */
    rop = (rop - 1) & 0x0f;
    *and = (color & rop2_and_array[rop][0]) | ((~color) & rop2_and_array[rop][1]);
    *xor = (color & rop2_xor_array[rop][0]) | ((~color) & rop2_xor_array[rop][1]);
}

/* ------------------------------------------------------------*/
/*                   ROP PIXEL FUNCTIONS                       */

static inline void do_rop_32(DWORD *ptr, DWORD and, DWORD xor)
{
    *ptr = (*ptr & and) ^ xor;
}

static inline void do_rop_16(WORD *ptr, WORD and, WORD xor)
{
    *ptr = (*ptr & and) ^ xor;
}

static inline void do_rop_8(BYTE *ptr, BYTE and, BYTE xor)
{
    *ptr = (*ptr & and) ^ xor;
}

/* ------------------------------------------------------------*/
/*                     HORIZONTAL LINES                        */

static void solid_hline_32(const DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    DWORD *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
        do_rop_32(ptr++, and, xor);
}

static void solid_hline_24(const DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE and_bytes[3], xor_bytes[3];

    and_bytes[0] =  and        & 0xff;
    and_bytes[1] = (and >> 8)  & 0xff;
    and_bytes[2] = (and >> 16) & 0xff;
    xor_bytes[0] =  xor        & 0xff;
    xor_bytes[1] = (xor >> 8)  & 0xff;
    xor_bytes[2] = (xor >> 16) & 0xff;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr++, and_bytes[0], xor_bytes[0]);
        do_rop_8(ptr++, and_bytes[1], xor_bytes[1]);
        do_rop_8(ptr++, and_bytes[2], xor_bytes[2]);
    }
}

static void solid_hline_16(const DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    WORD *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
        do_rop_16(ptr++, and, xor);
}

static void solid_hline_8(const DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
        do_rop_8(ptr++, and, xor);
}

static void solid_hline_4(const DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and, byte_xor;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);
    byte_and = (and & 0xf) | ((and << 4) & 0xf0);
    byte_xor = (xor & 0xf) | ((xor << 4) & 0xf0);

    if(start & 1) /* upper nibble untouched */
        do_rop_8(ptr++, byte_and | 0xf0, byte_xor & 0x0f);

    for(i = (start + 1) / 2; i < end / 2; i++)
        do_rop_8(ptr++, byte_and, byte_xor);

    if(end & 1) /* lower nibble untouched */
        do_rop_8(ptr, byte_and | 0x0f, byte_xor & 0xf0);
}

static void solid_hline_1(const DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and = 0, byte_xor = 0, mask;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    if(and & 1) byte_and = 0xff;
    if(xor & 1) byte_xor = 0xff;

    if((start & ~7) == (end & ~7)) /* special case run inside one byte */
    {
        mask = ((1L << ((end & 7) - (start & 7))) - 1) << (8 - (end & 7));
        do_rop_8(ptr, byte_and | ~mask, byte_xor & mask);
        return;
    }

    if(start & 7)
    {
        mask = (1 << (8 - (start & 7))) - 1;
        do_rop_8(ptr++, byte_and | ~mask, byte_xor & mask);
    }

    for(i = (start + 7) / 8; i < end / 8; i++)
        do_rop_8(ptr++, byte_and, byte_xor);

    if(end & 7)
    {
        mask = ~((1 << (8 - (end & 7))) - 1);
        do_rop_8(ptr++, byte_and | ~mask, byte_xor & mask);
    }
}

static void pattern_hline_32(const DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    DWORD *ptr;
    const DWORD *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset;
    xor_ptr += offset;
    for(i = start; i < end; i++)
    {
        do_rop_32(ptr++, *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_24(const DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset * 3;
    xor_ptr += offset * 3;

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr++,  *and_ptr++, *xor_ptr++);
        do_rop_8(ptr++,  *and_ptr++, *xor_ptr++);
        do_rop_8(ptr++,  *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_16(const DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    WORD *ptr;
    const WORD *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset;
    xor_ptr += offset;

    for(i = start; i < end; i++)
    {
        do_rop_16(ptr++, *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_8(const DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset;
    xor_ptr += offset;

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr++, *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_4(const DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;
    BYTE byte_and, byte_xor;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset / 2;
    xor_ptr += offset / 2;

    for(i = start; i < end; i++)
    {
        if(offset & 1)
        {
            byte_and = *and_ptr++ & 0x0f;
            byte_xor = *xor_ptr++ & 0x0f;
        }
        else
        {
            byte_and = (*and_ptr & 0xf0) >> 4;
            byte_xor = (*xor_ptr & 0xf0) >> 4;
        }

        if(i & 1)
            byte_and |= 0xf0;
        else
        {
            byte_and = (byte_and << 4) | 0x0f;
            byte_xor <<= 4;
        }

        do_rop_8(ptr, byte_and, byte_xor);

        if(i & 1) ptr++;

        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_1(const DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;
    BYTE byte_and, byte_xor, dst_mask, brush_mask;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset / 8;
    xor_ptr += offset / 8;

    for(i = start; i < end; i++)
    {
        dst_mask   = 1 << (7 - (i      & 7));
        brush_mask = 1 << (7 - (offset & 7));

        byte_and = (*and_ptr & brush_mask) ? 0xff : 0;
        byte_xor = (*xor_ptr & brush_mask) ? 0xff : 0;

        byte_and |= ~dst_mask;
        byte_xor &= dst_mask;

        do_rop_8(ptr, byte_and, byte_xor);

        if((i & 7) == 7) ptr++;
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
        else if((offset & 7) == 7)
        {
            and_ptr++;
            xor_ptr++;
        }
    }
}

/* ------------------------------------------------------------*/
/*                      VERTICAL LINES                         */

static void solid_vline_32(const DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_32((DWORD*)ptr, and, xor);
        ptr += dib->stride;
    }
}

static void solid_vline_24(const DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE and_bytes[3], xor_bytes[3];

    and_bytes[0] =  and        & 0xff;
    and_bytes[1] = (and >> 8)  & 0xff;
    and_bytes[2] = (and >> 16) & 0xff;
    xor_bytes[0] =  xor        & 0xff;
    xor_bytes[1] = (xor >> 8)  & 0xff;
    xor_bytes[2] = (xor >> 16) & 0xff;

    ptr  = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, and_bytes[0], xor_bytes[0]);
        do_rop_8(ptr + 1, and_bytes[1], xor_bytes[1]);
        do_rop_8(ptr + 2, and_bytes[2], xor_bytes[2]);
        ptr += dib->stride;
    }
}

static void solid_vline_16(const DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_16((WORD*)ptr, and, xor);
        ptr += dib->stride;
    }
}

static void solid_vline_8(const DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, and, xor);
        ptr += dib->stride;
    }
}

static void solid_vline_4(const DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and, byte_xor;

    if(col & 1) /* upper nibble untouched */
    {
        byte_and = (and & 0xf) | 0xf0;
        byte_xor = (xor & 0xf);
    }
    else
    {
        byte_and = ((and << 4) & 0xf0) | 0x0f;
        byte_xor = ((xor << 4) & 0xf0);
    }

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, byte_and, byte_xor);
        ptr += dib->stride;
    }
}

static void solid_vline_1(const DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and = 0, byte_xor = 0, mask;

    if(and & 1) byte_and = 0xff;
    if(xor & 1) byte_xor = 0xff;

    mask = 1 << (7 - (col & 7));

    byte_and |= ~mask;
    byte_xor &= mask;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, byte_and, byte_xor);
        ptr += dib->stride;
    }
}

/* ------------------------------------------------------------*/
/*                     PIXEL FUNCTIONS                         */

static void *get_pixel_ptr_32(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x * 4;
    return ptr;
}

static void *get_pixel_ptr_24(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x * 3;
    return ptr;
}

static void *get_pixel_ptr_16(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x * 2;
    return ptr;
}

static void *get_pixel_ptr_8(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x;
    return ptr;
}

static void *get_pixel_ptr_4(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x / 2;
    return ptr;
}

static void *get_pixel_ptr_1(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x / 8;
    return ptr;
}

static void set_pixel_32(const DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor)
{
    DWORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_32(ptr, and, xor);
}

static void set_pixel_24(const DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_8(ptr,      and        & 0xff,  xor        & 0xff);
    do_rop_8(ptr + 1, (and >> 8)  & 0xff, (xor >> 8)  & 0xff);
    do_rop_8(ptr + 2, (and >> 16) & 0xff, (xor >> 16) & 0xff);
}

static void set_pixel_16(const DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor)
{
    WORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_16(ptr, and, xor);
}

static void set_pixel_8(const DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_8(ptr, and, xor);
}

static void set_pixel_4(const DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    BYTE byte_and, byte_xor;

    if(x & 1) /* upper nibble untouched */
    {
        byte_and = (and & 0xf) | 0xf0;
        byte_xor = (xor & 0xf);
    }
    else
    {
        byte_and = ((and << 4) & 0xf0) | 0x0f;
        byte_xor = ((xor << 4) & 0xf0);
    }

    do_rop_8(ptr, byte_and, byte_xor);
}

static void set_pixel_1(const DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr;
    BYTE byte_and = 0, byte_xor = 0, mask;

    if(and & 1) byte_and = 0xff;
    if(xor & 1) byte_xor = 0xff;

    mask = 1 << (7 - (x & 7));

    byte_and |= ~mask;
    byte_xor &= mask;

    ptr = dib->funcs->get_pixel_ptr(dib, x, y);

    do_rop_8(ptr, byte_and, byte_xor);
}

static DWORD get_field32 (DWORD pixel, int shift, int len)
{
    pixel = pixel & (((1 << (len)) - 1) << shift);
    pixel = pixel << (32 - (shift + len)) >> 24;
    return pixel;
}

static DWORD get_field16 (WORD pixel, int shift, int len)
{
    FIXME("TODO\n");
    return 0;
}

static DWORD get_pixel_32_RGB(const DIBDRVBITMAP *dib, int x, int y)
{
    DWORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return *ptr;
}

static DWORD get_pixel_32_BITFIELDS(const DIBDRVBITMAP *dib, int x, int y)
{
    DWORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);

    return get_field32(*ptr, dib->red_shift,   dib->red_len)   << 16 |
           get_field32(*ptr, dib->green_shift, dib->green_len) <<  8 |
           get_field32(*ptr, dib->blue_shift,  dib->blue_len);
}

static DWORD get_pixel_24(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return (ptr[0] << 16) | (ptr[1] << 8) | ptr[2];
}

static DWORD get_pixel_16_RGB(const DIBDRVBITMAP *dib, int x, int y)
{
    WORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return ((*ptr & 0x7c00) << 9) | ((*ptr & 0x03e0) << 6) | ((*ptr & 0x001f) << 3);
}

static DWORD get_pixel_16_BITFIELDS(const DIBDRVBITMAP *dib, int x, int y)
{
    FIXME("TODO\n");
    return 0;
}

static DWORD get_pixel_8(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    RGBQUAD *color = dib->color_table + *ptr;
    return (color->rgbRed << 16) | (color->rgbGreen << 8) | color->rgbBlue;
}

static DWORD get_pixel_4(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y), pix;
    RGBQUAD *color;

    if(x & 1)
        pix = *ptr & 0x0f;
    else
        pix = *ptr >> 4;

    color = dib->color_table + pix;
    return (color->rgbRed << 16) | (color->rgbGreen << 8) | color->rgbBlue;
}

static DWORD get_pixel_1(const DIBDRVBITMAP *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y), pix;
    RGBQUAD *color;

    pix = *ptr;

    pix >>= (7 - (x & 7));
    pix &= 1;

    color = dib->color_table + pix;
    return (color->rgbRed << 16) | (color->rgbGreen << 8) | color->rgbBlue;
}

/* ------------------------------------------------------------*/
/*                     COLOR FUNCTIONS                         */

static inline DWORD put_field32(DWORD field, int shift, int len)
{
    shift = shift - (8 - len);
    if (len <= 8)
        field &= (((1 << len) - 1) << (8 - len));
    if (shift < 0)
        field >>= -shift;
    else
        field <<= shift;
    return field;
}

static inline WORD put_field16(DWORD field, int shift, int len)
{
    FIXME("TODO\n");
    return 0;
}

static DWORD colorref_to_pixel_32_RGB(const DIBDRVBITMAP *dib, COLORREF color)
{
    return ( ((color >> 16) & 0xff) | (color & 0xff00) | ((color << 16) & 0xff0000) );
}

static DWORD colorref_to_pixel_32_BITFIELDS(const DIBDRVBITMAP *dib, COLORREF color)
{
    DWORD r,g,b;

    r = GetRValue(color);
    g = GetGValue(color);
    b = GetBValue(color);

    return put_field32(r, dib->red_shift,   dib->red_len) |
           put_field32(g, dib->green_shift, dib->green_len) |
           put_field32(b, dib->blue_shift,  dib->blue_len);
}

static DWORD colorref_to_pixel_24(const DIBDRVBITMAP *dib, COLORREF color)
{
    return ( ((color >> 16) & 0xff) | (color & 0xff00) | ((color << 16) & 0xff0000) );
}

static DWORD colorref_to_pixel_16_RGB(const DIBDRVBITMAP *dib, COLORREF color)
{
    return ( ((color >> 19) & 0x001f) | ((color >> 6) & 0x03e0) | ((color << 7) & 0x7c00) );
}

static DWORD colorref_to_pixel_16_BITFIELDS(const DIBDRVBITMAP *dib, COLORREF color)
{
    FIXME("TODO\n");
    return 0;
}

static DWORD colorref_to_pixel_colortable(const DIBDRVBITMAP *dib, COLORREF color)
{
    int i, best_index = 0;
    DWORD r, g, b;
    DWORD diff, best_diff = 0xffffffff;

    r = GetRValue(color);
    g = GetGValue(color);
    b = GetBValue(color);

    for(i = 0; i < dib->color_table_size; i++)
    {
        RGBQUAD *cur = dib->color_table + i;
        diff = (r - cur->rgbRed)   * (r - cur->rgbRed)
            +  (g - cur->rgbGreen) * (g - cur->rgbGreen)
            +  (b - cur->rgbBlue)  * (b - cur->rgbBlue);

        if(diff == 0)
        {
            best_index = i;
            break;
        }

        if(diff < best_diff)
        {
            best_diff = diff;
            best_index = i;
        }
    }
    return best_index;
}

/* ----------------------------------------------------------------*/
/*                         CONVERT PRIMITIVES                      */
/* converts (part of) line of any DIB format from/to DIB32_RGB one */
static void GetLine_32_RGB(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    DWORD *src = (DWORD *)((BYTE *)bmp->bits + line * bmp->stride + 4 * startx);
    for(; width; width--)
        *dwBuf++ = *src++;
}

static void GetLine_32_BITFIELDS(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    BYTE *bBuf = (BYTE *)buf;
    DWORD *src = (DWORD *)((BYTE *)bmp->bits + line * bmp->stride + 4 * startx);
    for(; width ; width--)
    {
        *bBuf++ = (*src & bmp->blue_mask ) >> bmp->blue_shift;
        *bBuf++ = (*src & bmp->green_mask) >> bmp->green_shift;
        *bBuf++ = (*src & bmp->red_mask  ) >> bmp->red_shift;
        *bBuf++ = 0x0;
        src++;
    }
}

static void GetLine_24(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    BYTE *bBuf = (BYTE *)buf;
    BYTE *src = ((BYTE *)bmp->bits + line * bmp->stride + 3 * startx);
    for(; width ; width--)
    {
        *bBuf++ = *src++;
        *bBuf++ = *src++;
        *bBuf++ = *src++;
        *bBuf++ = 0x0;
    }
}

static void GetLine_16_RGB(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *bBuf = (DWORD *)buf;
    WORD *src = (WORD *)((BYTE *)bmp->bits + line * bmp->stride + 2 * startx);
    DWORD b;
    for(; width ; width--)
    {
        b = *src++;
        /* 0RRR|RRGG|GGGB|BBBB */
        *bBuf++ = (b & 0x1f) | ((b & 0x3e0) << 3) | ((b & 0x7c00) << 6);
    }
}

static void GetLine_16_BITFIELDS(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *bBuf = (DWORD *)buf;
    WORD *src = (WORD *)((BYTE *)bmp->bits + line * bmp->stride + 2 * startx);
    DWORD b;
    for(; width ; width--)
    {
        b = *src++;
        *bBuf++ = (( b & bmp->blue_mask ) >> bmp->blue_shift) |
                  (((b & bmp->green_mask) >> bmp->green_shift) << 8) |
                  (((b & bmp->red_mask  ) >> bmp->red_shift) << 16);
    }
}

static void GetLine_8(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *src = ((BYTE *)bmp->bits + line * bmp->stride + startx);
    for(; width ; width--)
        *dwBuf++ = *((DWORD *)bmp->color_table + *src++);
}

static void GetLine_4(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *src = ((BYTE *)bmp->bits + line * bmp->stride + (startx >> 1));
    
    /* if startx is odd, get first nibble */
    if(startx & 0x01)
    {
        *dwBuf++ = *((DWORD *)bmp->color_table + ((*src++ >> 4) & 0x0f));
        width--;
    }
    
    /* then gets all full image bytes */
    for( ; width > 1 ; width -= 2)
    {
        *dwBuf++ = *((DWORD *)bmp->color_table + (*src & 0x0f));
        *dwBuf++ = *((DWORD *)bmp->color_table + ((*src++ >> 4) & 0x0f));
    }
    
    /* last nibble, if any */
    if(width)
        *dwBuf++ = *((DWORD *)bmp->color_table + (*src & 0x0f));
}

static void GetLine_1(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *src = ((BYTE *)bmp->bits + line * bmp->stride + (startx >> 3));
    BYTE b;
    char i;
    DWORD pixOn  = *((DWORD *)bmp->color_table + 1);
    DWORD pixOff = *(DWORD *)bmp->color_table;
    
    /* get first partial byte, if any */
    startx = (8 - (startx & 0x03)) & 0x03;
    width -= startx;
    if(startx)
    {
        b = *src++ >> (8 - startx);
        while(startx--)
        {
            if(b & 0x01)
                *dwBuf++ = pixOn;
            else
                *dwBuf++ = pixOff;
            b >>= 1;
        }
    }
    
    /* then gets full next bytes */
    for( ; width > 7 ; width -= 8)
    {
        b = *src++;
        for(i = 0 ; i < 8 ; i++)
        {
            if(b & 0x01)
                *dwBuf++ = pixOn;
            else
                *dwBuf++ = pixOff;
            b >>= 1;
        }
    }
    
    /* last partial byte, if any */
    if(width)
    {
        b = *src;
        while(width--)
        {
            if(b & 0x01)
                *dwBuf++ = pixOn;
            else
                *dwBuf++ = pixOff;
            b >>= 1;
        }
    }
}

static void PutLine_32_RGB(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    DWORD *dst = (DWORD *)((BYTE *)bmp->bits + line * bmp->stride + 4 * startx);
    for(; width; width--)
        *dst++ = *dwBuf++;
}

static void PutLine_32_BITFIELDS(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    DWORD *dst = (DWORD *)((BYTE *)bmp->bits + line * bmp->stride + 4 * startx);
    DWORD c;
    for(; width; width--)
    {
        c = *dwBuf++;
        *dst++ =
            ((( c & 0x000000ff)        << bmp->blue_shift)  & bmp->blue_mask) |
            ((((c & 0x0000ff00) >>  8) << bmp->green_shift) & bmp->green_mask) |
            ((((c & 0x00ff0000) >> 16) << bmp->red_shift)   & bmp->red_mask);
    }
}

static void PutLine_24(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *dst = ((BYTE *)bmp->bits + line * bmp->stride + 3 * startx);
    DWORD c;
    for(; width; width--)
    {
        c = *dwBuf++;
        *dst++ = c &  0x000000ff;
        *dst++ = c & (0x0000ff00 >>  8);
        *dst++ = c & (0x00ff0000 >> 16);
    }
}

static void PutLine_16_RGB(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    WORD *dst = (WORD *)((BYTE *)bmp->bits + line * bmp->stride + 2 * startx);
    DWORD c;
    for(; width; width--)
    {
        c = *dwBuf++;
        *dst++ =
            ((c & 0x000000f8) >> 3) |
            ((c & 0x0000f800) >> 6) |
            ((c & 0x00f80000) >> 9);
    }
}

static void PutLine_16_BITFIELDS(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    WORD *dst = (WORD *)((BYTE *)bmp->bits + line * bmp->stride + 2 * startx);
    DWORD c;
    for(; width; width--)
    {
        c = *dwBuf++;
        *dst++ =
            ((( c & 0x000000ff)        << bmp->blue_shift)  & bmp->blue_mask) |
            ((((c & 0x0000ff00) >>  8) << bmp->green_shift) & bmp->green_mask) |
            ((((c & 0x00ff0000) >> 16) << bmp->red_shift)   & bmp->red_mask);
    }
}

static void PutLine_8(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *dst = ((BYTE *)bmp->bits + line * bmp->stride + startx);
    DWORD c;
    DWORD last_color = 0xffffffff;
    int last_index = -1;

    for(; width; width--)
    {
        c = *dwBuf++;
        
        /* slight optimization, as images often have many
           consecutive pixels with same color */
        if(last_index == -1 || c != last_color)
        {
            last_index = colorref_to_pixel_colortable(bmp, c);
            last_color = c;
        }
        *dst++ = last_index;
    }
}

static void PutLine_4(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *dst = ((BYTE *)bmp->bits + line * bmp->stride + (startx >> 1));
    DWORD c;
    DWORD last_color = 0xffffffff;
    int last_index = -1;
    
    /* if startx is odd, put first nibble */
    if(startx & 0x01)
    {
        c = *dwBuf++;
        
        last_index = colorref_to_pixel_colortable(bmp, c);
        last_color = c;
        *dst = (*dst & 0x0f) | (last_index << 4);
        dst++;
        width--;
    }
    
    /* then gets all full image bytes */
    for( ; width > 1 ; width -= 2)
    {
        c = *dwBuf++;
        
        /* slight optimization, as images often have many
           consecutive pixels with same color */
        if(last_index == -1 || c != last_color)
        {
            last_index = colorref_to_pixel_colortable(bmp, c);
            last_color = c;
        }
        *dst = last_index;
        
        c = *dwBuf++;
        
        /* slight optimization, as images often have many
           consecutive pixels with same color */
        if(last_index == -1 || c != last_color)
        {
            last_index = colorref_to_pixel_colortable(bmp, c);
            last_color = c;
        }
        *dst++ |= last_index << 4;
    }
    
    /* last nibble, if any */
    if(width)
    {
        c = *dwBuf;
        
        /* slight optimization, as images often have many
           consecutive pixels with same color */
        if(last_index == -1 || c != last_color)
            last_index = colorref_to_pixel_colortable(bmp, c);
        *dst = (*dst & 0xf0) | last_index;
    }
}

static void PutLine_1(const DIBDRVBITMAP *bmp, INT line, INT startx, int width, void *buf)
{
    DWORD *dwBuf = (DWORD *)buf;
    BYTE *dst = ((BYTE *)bmp->bits + line * bmp->stride + (startx >> 3));
    BYTE b, mask;
    char i;
    
    /* put first partial byte, if any */
    startx &= 0x03;
    mask = 1 << startx;
    startx = (8 - startx) & 0x03;
    if(startx)
    {
        width -= startx;
        b = *dst;
        while(startx--)
        {
            if(dwBuf++)
                b |= mask;
            else
                b &= !mask;
            mask <<= 1;
        }
        *dst++ = b; 
    }
    
    /* then puts full next bytes */
    for( ; width > 7 ; width -= 8)
    {
        b = 0;
        mask = 0x01;
        for(i = 0 ; i < 8 ; i++)
        {
            if(*dwBuf++)
                b |= mask;
            mask <<= 1;
        }
        *dst++ = b;
    }
    
    /* last partial byte, if any */
    if(width)
    {
        b = *dst;
        mask = 1;
        while(width--)
        {
            if(*dwBuf++)
                b |= mask;
            else
                b &= !mask;
            mask <<= 1;
        }
        *dst = b;
    }
}

/* ------------------------------------------------------------*/
/*                        BLITTING PRIMITIVES                  */

static void BitBlt_generic( const DIBDRVBITMAP *dstBmp, INT xDst, INT yDst,
                    INT width, INT height, const DIBDRVBITMAP *srcBmp,
                    INT xSrc, INT ySrc, DWORD rop )
{
    int ys, yd;
    int i;
    DWORD *dwBuf;
    DWORD *wDstPnt, *wSrcPnt;
   
    /* 32 bit RGB source and destination buffer, if needed */
    DWORD *sBuf;
    
    /* allocate 32 bit RGB destination buffer */
    DWORD *dBuf = (DWORD *)malloc(width * 4);
    
    TRACE("dstBmp:%p(%s), xDst:%d, yDst:%d, width:%d, height:%d, srcBmp:%p(%s), xSrc:%d, ySrc:%d, rop:%8x\n",
          dstBmp, _DIBDRV_dib_format_name(dstBmp), xDst, yDst, width, height, 
          srcBmp, _DIBDRV_dib_format_name(srcBmp), xSrc, ySrc, rop);
    
    /* some simple ROPs optimizations */
    switch(rop)
    {
          case BLACKNESS:
              memset(dBuf, 0x00, width * 4);
              for(yd = yDst; yd < yDst+height; yd++)
                  dstBmp->funcs->put_line(dstBmp, yd, xDst, width, dBuf);
              break;
          
          case WHITENESS:
              for(dwBuf = dBuf; width; width++)
                  *dwBuf++ = 0x00ffffff;
              for(yd = yDst; yd < yDst+height; yd++)
                  dstBmp->funcs->put_line(dstBmp, yd, xDst, width, dBuf);
              break;
          
          case SRCCOPY:
              for(ys = ySrc, yd = yDst; ys < ySrc+height; ys++, yd++)
              {
                  srcBmp->funcs->get_line(srcBmp, ys, xSrc, width, dBuf);
                  dstBmp->funcs->put_line(dstBmp, yd, xDst, width, dBuf);
              }
              break;
              
          /* fallback for generic ROP operation */
          default:
              FIXME("Add pattern handling\n");
              /* allocate 32 bit RGB source buffer */
              sBuf = (DWORD *)malloc(width * 4);
              rop >>= 16;
              for(ys = ySrc, yd = yDst; ys < ySrc+height; ys++, yd++)
              {
                  srcBmp->funcs->get_line(srcBmp, ys, xSrc, width, sBuf);
                  dstBmp->funcs->get_line(dstBmp, yd, xDst, width, dBuf);

                  wDstPnt = dBuf;
                  wSrcPnt = sBuf;
                  for(i = width; i > 0 ; i--)
                  {
                      *wDstPnt = DIBDRV_ROP3(0, *wSrcPnt++, *wDstPnt, rop);
                      wDstPnt++;
                  }
                  dstBmp->funcs->put_line(dstBmp, yd, xDst, width, dBuf);
              }
              free(sBuf);
              break;
    } /* switch */

    free(dBuf);
}

static void BitBlt_32( const DIBDRVBITMAP *dstBmp, INT xDst, INT yDst,
                    INT width, INT height, const DIBDRVBITMAP *srcBmp,
                    INT xSrc, INT ySrc, DWORD rop )
{
    int ys, yd, i;
    BYTE *srcPnt, *dstPnt;
    DWORD *wDstPnt, *wSrcPnt;
    int lineSize;
    
    TRACE("dstBmp:%p(%s), xDst:%d, yDst:%d, width:%d, height:%d, srcBmp:%p(%s), xSrc:%d, ySrc:%d, rop:%8x\n",
          dstBmp, _DIBDRV_dib_format_name(dstBmp), xDst, yDst, width, height, 
          srcBmp, _DIBDRV_dib_format_name(srcBmp), xSrc, ySrc, rop);
    
    /* fall case, if source and dest bmp formats don't match */
    if(srcBmp->format != dstBmp->format)
    {
        BitBlt_generic( dstBmp, xDst, yDst, width, height, srcBmp, xSrc, ySrc, rop );
        return;
    }
    
    /* identical formats, optimized blitting */
    if(dstBmp)
        dstPnt = (BYTE *)dstBmp->bits + yDst * dstBmp->stride + 4 * xDst;
    else
        dstPnt = 0;
    if(srcBmp)
        srcPnt = (BYTE *)srcBmp->bits + ySrc * srcBmp->stride + 4 * xSrc;
    else
        srcPnt = 0;
    lineSize = 4 * width;

    /* some simple ROPs optimizations */
    switch(rop)
    {
          case BLACKNESS:
              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
              {
                  wDstPnt = (DWORD *)dstPnt;
                  for(i = width; i > 0 ; i--)
                      *wDstPnt++ = 0x00000000;
              }
              break;
          
          case WHITENESS:
              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
              {
                  wDstPnt = (DWORD *)dstPnt;
                  for(i = width; i > 0 ; i--)
                      *wDstPnt++ = 0x00ffffff;
              }
              break;
          
          case SRCCOPY:
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
                  memmove(dstPnt, srcPnt, lineSize);
              break;
              
          /* fallback for generic ROP operation */
          default:
              FIXME("Add pattern handling\n");
              rop >>= 16;
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
              {
                  wDstPnt = (DWORD *)dstPnt;
                  wSrcPnt = (DWORD *)srcPnt;
                  for(i = width; i > 0 ; i--)
                  {
                      *wDstPnt = DIBDRV_ROP3(0, *wSrcPnt++, *wDstPnt, rop);
                      wDstPnt++;
                  }
              }
              break;
    } /* switch */
}

static void BitBlt_24( const DIBDRVBITMAP *dstBmp, INT xDst, INT yDst,
                    INT width, INT height, const DIBDRVBITMAP *srcBmp,
                    INT xSrc, INT ySrc, DWORD rop )
{
    int ys, yd, i;
    BYTE *srcPnt, *dstPnt;
    BYTE *bDstPnt, *bSrcPnt;
    int lineSize;
    
    TRACE("dstBmp:%p(%s), xDst:%d, yDst:%d, width:%d, height:%d, srcBmp:%p(%s), xSrc:%d, ySrc:%d, rop:%8x\n",
          dstBmp, _DIBDRV_dib_format_name(dstBmp), xDst, yDst, width, height, 
          srcBmp, _DIBDRV_dib_format_name(srcBmp), xSrc, ySrc, rop);
    
    /* fall case, if source and dest bmp formats don't match */
    if(srcBmp->format != dstBmp->format)
    {
        BitBlt_generic( dstBmp, xDst, yDst, width, height, srcBmp, xSrc, ySrc, rop );
        return;
    }
    
    /* identical formats, optimized blitting */
    if(dstBmp)
        dstPnt = (BYTE *)dstBmp->bits + yDst * dstBmp->stride + 3 * xDst;
    else
        dstPnt = 0;
    if(srcBmp)
        srcPnt = (BYTE *)srcBmp->bits + ySrc * srcBmp->stride + 3 * xSrc;
    else
        srcPnt = 0;
    lineSize = 3 * width;

    /* some simple ROPs optimizations */
    switch(rop)
    {
          case BLACKNESS:
              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
                  memset(dstPnt, 0x00, lineSize);
              break;
          
          case WHITENESS:
              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
                  memset(dstPnt, 0xff, lineSize);
              break;
          
          case SRCCOPY:
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
                  memmove(dstPnt, srcPnt, lineSize);
              break;
              
          /* fallback for generic ROP operation */
          default:
              FIXME("Add pattern handling\n");
              rop >>= 16;
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
              {
                  bDstPnt = dstPnt;
                  bSrcPnt = srcPnt;
                  for(i = 3 * width; i > 0 ; i--)
                  {
                      *bDstPnt = DIBDRV_ROP3(0, *bSrcPnt++, *bDstPnt, rop);
                      bDstPnt++;
                  }
              }
              break;
    } /* switch */
}

static void BitBlt_16( const DIBDRVBITMAP *dstBmp, INT xDst, INT yDst,
                    INT width, INT height, const DIBDRVBITMAP *srcBmp,
                    INT xSrc, INT ySrc, DWORD rop )
{
    int ys, yd, i;
    BYTE *srcPnt, *dstPnt;
    WORD *wDstPnt, *wSrcPnt;
    int lineSize;
    
    TRACE("dstBmp:%p(%s), xDst:%d, yDst:%d, width:%d, height:%d, srcBmp:%p(%s), xSrc:%d, ySrc:%d, rop:%8x\n",
          dstBmp, _DIBDRV_dib_format_name(dstBmp), xDst, yDst, width, height, 
          srcBmp, _DIBDRV_dib_format_name(srcBmp), xSrc, ySrc, rop);
    
    /* fall case, if source and dest bmp formats don't match */
    if(srcBmp->format != dstBmp->format)
    {
        BitBlt_generic( dstBmp, xDst, yDst, width, height, srcBmp, xSrc, ySrc, rop );
        return;
    }
    
    /* identical formats, optimized blitting */
    if(dstBmp)
        dstPnt = (BYTE *)dstBmp->bits + yDst * dstBmp->stride + 2 * xDst;
    else
        dstPnt = 0;
    if(srcBmp)
        srcPnt = (BYTE *)srcBmp->bits + ySrc * srcBmp->stride + 2 * xSrc;
    else
        srcPnt = 0;
    lineSize = 2 * width;

    /* some simple ROPs optimizations */
    switch(rop)
    {
          case BLACKNESS:
              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
              {
                  wDstPnt = (WORD *)dstPnt;
                  for(i = width; i > 0 ; i--)
                      *wDstPnt++ = 0x0000;
              }
              break;
          
          case WHITENESS:
              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
              {
                  wDstPnt = (WORD *)dstPnt;
                  for(i = width; i > 0 ; i--)
                      *wDstPnt++ = 0xffff;
              }
              break;
          
          case SRCCOPY:
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
                  memmove(dstPnt, srcPnt, lineSize);
              break;
              
          /* fallback for generic ROP operation */
          default:
              FIXME("Add pattern handling\n");
              rop >>= 16;
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
              {
                  wDstPnt = (WORD *)dstPnt;
                  wSrcPnt = (WORD *)srcPnt;
                  for(i = width; i > 0 ; i--)
                  {
                      *wDstPnt = DIBDRV_ROP3(0, *wSrcPnt++, *wDstPnt, rop);
                      wDstPnt++;
                  }
              }
              break;
    } /* switch */
}

static void BitBlt_8( const DIBDRVBITMAP *dstBmp, INT xDst, INT yDst,
                    INT width, INT height, const DIBDRVBITMAP *srcBmp,
                    INT xSrc, INT ySrc, DWORD rop )
{
    int ys, yd, i;
    BYTE *srcPnt, *dstPnt;
    BYTE *bDstPnt, *bSrcPnt;
    int lineSize;
    int colorIndex;
    
    TRACE("dstBmp:%p(%s), xDst:%d, yDst:%d, width:%d, height:%d, srcBmp:%p(%s), xSrc:%d, ySrc:%d, rop:%8x\n",
          dstBmp, _DIBDRV_dib_format_name(dstBmp), xDst, yDst, width, height, 
          srcBmp, _DIBDRV_dib_format_name(srcBmp), xSrc, ySrc, rop);
    
    /* fall case, if source and dest bmp formats don't match */
    if(!_DIBDRV_dib_formats_match(srcBmp, dstBmp))
    {
        BitBlt_generic( dstBmp, xDst, yDst, width, height, srcBmp, xSrc, ySrc, rop );
        return;
    }
    
    /* identical formats, optimized blitting */
    if(dstBmp)
        dstPnt = (BYTE *)dstBmp->bits + yDst * dstBmp->stride + xDst;
    else
        dstPnt = 0;
    if(srcBmp)
        srcPnt = (BYTE *)srcBmp->bits + ySrc * srcBmp->stride + xSrc;
    else
        srcPnt = 0;
    lineSize = width;

    /* some simple ROPs optimizations */
    switch(rop)
    {
          case BLACKNESS:
              /* gets black color index */
              colorIndex = colorref_to_pixel_colortable(dstBmp, 0x00000000);

              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
              {
                  bDstPnt = dstPnt;
                  for(i = width; i > 0 ; i--)
                      *bDstPnt++ = colorIndex;
              }
              break;
          
          case WHITENESS:
              /* gets white color index */
              colorIndex = colorref_to_pixel_colortable(dstBmp, 0x00ffffff);

              for(yd = yDst; yd < yDst+height; yd++, dstPnt += dstBmp->stride)
              {
                  bDstPnt = dstPnt;
                  for(i = width; i > 0 ; i--)
                      *bDstPnt++ = colorIndex;
              }
              break;
          
          case SRCCOPY:
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
                  memmove(dstPnt, srcPnt, lineSize);
              break;
              
          /* fallback for generic ROP operation */
          default:
              FIXME("Add pattern handling\n");
              rop >>= 16;
              for(ys = ySrc; ys < ySrc+height; ys++, srcPnt += srcBmp->stride, dstPnt += dstBmp->stride)
              {
                  bDstPnt = dstPnt;
                  bSrcPnt = srcPnt;
                  for(i = width; i > 0 ; i--)
                  {
                      *bDstPnt = DIBDRV_ROP3(0, *bSrcPnt++, *bDstPnt, rop);
                      bDstPnt++;
                  }
              }
              break;
    } /* switch */
}


/* ------------------------------------------------------------*/
/*               FREETYPE FONT BITMAP BLITTING                 */

static void freetype_blit_8888(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;
    DWORD *ptr;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        ptr = (DWORD *)((BYTE *)dib->bits + (dibY * dib->stride) + x * 4);
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                *ptr = c;
            }
            buf++;
            ptr++;
        }
    }
}

static void freetype_blit_32_RGB(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_32_BITFIELDS(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_24(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_16_RGB(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_16_BITFIELDS(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_8(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_4(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}

static void freetype_blit_1(const DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp)
{
    /* FIXME : MUST BE OPTIMIZED !!! */
    
    INT bmpX, bmpY;
    BYTE *buf;
    INT dibX, dibY;
    INT DIBX, DIBY;
    DWORD c;

    /* gets DIB limits */
    DIBX = dib->width;
    DIBY = dib->height;

    /* loop for every pixel in bitmap */
    buf = bmp->buffer;
    for(bmpY = 0, dibY = y; bmpY < bmp->rows; bmpY++, dibY++)
    {
        for(bmpX = 0, dibX = x; bmpX < bmp->width; bmpX++, dibX++)
        {
            if(dibX < DIBX && dibY < DIBY && dibX > 0 && dibY > 0 && *buf)
            {
                c = dib->textColorTable[*buf];
                dib->funcs->set_pixel(dib, dibX, dibY, 0, c);
            }
            buf++;
        }
    }
}


DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB32_RGB =
{
    solid_hline_32,
    pattern_hline_32,
    solid_vline_32,
    get_pixel_ptr_32,
    set_pixel_32,
    get_pixel_32_RGB,
    colorref_to_pixel_32_RGB,
    GetLine_32_RGB,
    PutLine_32_RGB,
    BitBlt_32,
    freetype_blit_32_RGB
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB32_BITFIELDS =
{
    solid_hline_32,
    pattern_hline_32,
    solid_vline_32,
    get_pixel_ptr_32,
    set_pixel_32,
    get_pixel_32_BITFIELDS,
    colorref_to_pixel_32_BITFIELDS,
    GetLine_32_BITFIELDS,
    PutLine_32_BITFIELDS,
    BitBlt_32,
    freetype_blit_32_BITFIELDS
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB24 =
{
    solid_hline_24,
    pattern_hline_24,
    solid_vline_24,
    get_pixel_ptr_24,
    set_pixel_24,
    get_pixel_24,
    colorref_to_pixel_24,
    GetLine_24,
    PutLine_24,
    BitBlt_24,
    freetype_blit_24
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB16_RGB =
{
    solid_hline_16,
    pattern_hline_16,
    solid_vline_16,
    get_pixel_ptr_16,
    set_pixel_16,
    get_pixel_16_RGB,
    colorref_to_pixel_16_RGB,
    GetLine_16_RGB,
    PutLine_16_RGB,
    BitBlt_16,
    freetype_blit_16_RGB
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB16_BITFIELDS =
{
    solid_hline_16,
    pattern_hline_16,
    solid_vline_16,
    get_pixel_ptr_16,
    set_pixel_16,
    get_pixel_16_BITFIELDS,
    colorref_to_pixel_16_BITFIELDS,
    GetLine_16_BITFIELDS,
    PutLine_16_BITFIELDS,
    BitBlt_16,
    freetype_blit_16_BITFIELDS
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB8 =
{
    solid_hline_8,
    pattern_hline_8,
    solid_vline_8,
    get_pixel_ptr_8,
    set_pixel_8,
    get_pixel_8,
    colorref_to_pixel_colortable,
    GetLine_8,
    PutLine_8,
    BitBlt_8,
    freetype_blit_8
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB4 =
{
    solid_hline_4,
    pattern_hline_4,
    solid_vline_4,
    get_pixel_ptr_4,
    set_pixel_4,
    get_pixel_4,
    colorref_to_pixel_colortable,
    GetLine_4,
    PutLine_4,
    BitBlt_generic,
    freetype_blit_4
};

DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB1 =
{
    solid_hline_1,
    pattern_hline_1,
    solid_vline_1,
    get_pixel_ptr_1,
    set_pixel_1,
    get_pixel_1,
    colorref_to_pixel_colortable,
    GetLine_1,
    PutLine_1,
    BitBlt_generic,
    freetype_blit_1
};
