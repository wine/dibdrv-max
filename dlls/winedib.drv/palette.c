/*
 * DIBDRV palette objects
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/***********************************************************************
 *              DIBDRV_RealizePalette
 */
UINT DIBDRV_RealizePalette( DIBDRVPHYSDEV *physDev, HPALETTE hpal, BOOL primary )
{
    TRACE("physDev:%p, hpal:%p, primary:%s\n", physDev, hpal, (primary ? "TRUE" : "FALSE"));
    
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return 0;
}

/***********************************************************************
 *              DIBDRV_UnrealizePalette
 */
BOOL DIBDRV_UnrealizePalette( HPALETTE hpal )
{
    TRACE("hpal:%p\n", hpal);
    
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *              DIBDRV_GetSystemPaletteEntries
 */
UINT DIBDRV_GetSystemPaletteEntries( DIBDRVPHYSDEV *physDev, UINT start, UINT count,
                                     LPPALETTEENTRY entries )
{
    TRACE("physDev:%p, start:%d, count:%d, entries:%p\n", physDev, start, count, entries);
    
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return 0;
}

/***********************************************************************
 *              DIBDRV_GetNearestColor
 */
COLORREF DIBDRV_GetNearestColor( DIBDRVPHYSDEV *physDev, COLORREF color )
{
    TRACE("physDev:%p, color:%x\n", physDev, color);
    
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return color;
}

/***********************************************************************
 *              DIBDRV_RealizeDefaultPalette
 */
UINT DIBDRV_RealizeDefaultPalette( DIBDRVPHYSDEV *physDev )
{
    int i;
    RGBQUAD *q;
    
    TRACE("physDev:%p\n", physDev);
    
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif

    /* HACK - we can't get the dib color table during SelectBitmap since it hasn't
       been initialized yet.  This is called from DC_InitDC so it's a convenient place
       to grab the color table. */
    TRACE("Color table size = %d, Color table = %p\n", physDev->bmp.color_table_size, physDev->bmp.color_table);
    if(physDev->bmp.color_table_size && !physDev->bmp.color_table)
    {
        TRACE("Grabbing palette\n");
        physDev->bmp.color_table = HeapAlloc(GetProcessHeap(), 0, sizeof(physDev->bmp.color_table[0]) * physDev->bmp.color_table_size);
        GetDIBColorTable(physDev->hdc, 0, physDev->bmp.color_table_size, physDev->bmp.color_table);
        for(i = 0; i < physDev->bmp.color_table_size; i++)
        {
            q = physDev->bmp.color_table + i;
            TRACE("  %03d : R%03d G%03d B%03d\n", i, q->rgbRed, q->rgbGreen, q->rgbBlue);
        }
    }
    return 0;
}
