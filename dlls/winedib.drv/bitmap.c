/*
 * DIB driver bitmap objects
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Huw Davies
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/* gets human-readable dib format name */
const char *_DIBDRV_dib_format_name(DIBDRVBITMAP const *bmp)
{
   switch(bmp->format)
   {
       case DIBFMT_DIB1:
           return "DIBFMT_DIB1";
           
       case DIBFMT_DIB4:
           return "DIBFMT_DIB4";
           
       case DIBFMT_DIB4_RLE:
           return "DIBFMT_DIB4_RLE";
           
       case DIBFMT_DIB8:
           return "DIBFMT_DIB8";
           
       case DIBFMT_DIB8_RLE:
           return "DIBFMT_DIB8_RLE";
           
       case DIBFMT_DIB16_RGB:
           return "DIBFMT_DIB_RGB";
           
       case DIBFMT_DIB16_BITFIELDS:
           return "DIBFMT_DIB16_BITFIELDS";
           
       case DIBFMT_DIB24:
           return "DIBFMT_DIB24";
           
       case DIBFMT_DIB32_RGB:
           return "DIBFMT_DIB32_RGB";
           
       case DIBFMT_DIB32_BITFIELDS:
           return "DIBFMT_DIB32_BITFIELDS";
           
       case DIBFMT_UNKNOWN:
           return "DIBFMT_UNKNOWN";
   }
          
}

/* calculates shift and length given a bit mask */
static void calc_shift_and_len(DWORD mask, int *shift, int *len)
{
    int s, l;

    /* FIXME----*/
    if(mask == 0)
    {
        FIXME("color mask == 0 -- problem on init_dib\n");
        *shift = 0;
        *len = 0;
        return;
    }
    
    /* calculates bit shift
       (number of 0's on right of bit field */
    s = 0;
    while ((mask & 1) == 0)
    {
        mask >>= 1;
        s++;
    }

    /* calculates bitfield length
       (number of 1's in bit field */
    l = 0;
    while ((mask & 1) == 1)
    {
        mask >>= 1;
        l++;
    }
    *shift = s;
    *len = l;
}

/* initializes bit fields from bit masks */
static void init_bit_fields(DIBDRVBITMAP *dib, const DWORD *bit_fields)
{
    dib->red_mask    = bit_fields[0];
    dib->green_mask  = bit_fields[1];
    dib->blue_mask   = bit_fields[2];
    calc_shift_and_len(dib->red_mask,   &dib->red_shift,   &dib->red_len);
    calc_shift_and_len(dib->green_mask, &dib->green_shift, &dib->green_len);
    calc_shift_and_len(dib->blue_mask,  &dib->blue_shift,  &dib->blue_len);
}

/* initializes dib from a bitmap : 
    dib           dib being initialized
    bi            source BITMAPINFOHEADER with required DIB format info
    bit_fields    color masks
    color_table   color table, if any
    bits          pointer to image data array
*/
BOOL _DIBDRV_init_dib(DIBDRVBITMAP *dib, const BITMAPINFOHEADER *bi, const DWORD *bit_fields,
                     const RGBQUAD *color_table, void *bits)
{
    /* initializes DIB dimensions and color depth */
    dib->bit_count = bi->biBitCount;
    dib->width     = bi->biWidth;
    dib->height    = bi->biHeight;
    dib->stride    = ((dib->width * dib->bit_count + 31) >> 3) & ~3;

    /* initializes image data pointer */
    dib->bits      = bits;

    /* initializes color table */
    dib->color_table_size = 0;
    dib->color_table = NULL;

    /* checks whether dib is top-down or bottom-up one */
    if(dib->height < 0)
    {
        /* top-down dib */
        dib->height = -dib->height;
    }
    else
    {
        /* bottom-up dib */
        /* data->bits always points to the top-left corner and the stride is -ve */
        dib->bits    = (BYTE*)dib->bits + (dib->height - 1) * dib->stride;
        dib->stride  = -dib->stride;
    }

    /* gets and stores bitmap format */
    switch(dib->bit_count)
    {
        case 24:
            dib->format = DIBFMT_DIB24;
            dib->funcs = &DIBDRV_funcs_DIB24;
            break;
    
        case 32:
    
            if(bi->biCompression == BI_RGB)
            {
                dib->format = DIBFMT_DIB32_RGB;
                dib->funcs = &DIBDRV_funcs_DIB32_RGB;
            }
            else
            {
                init_bit_fields(dib, bit_fields);
                dib->format = DIBFMT_DIB32_BITFIELDS;
                dib->funcs = &DIBDRV_funcs_DIB32_BITFIELDS;
            }
            break;
    
        case 16:
            if(bi->biCompression == BI_RGB)
            {
                dib->format = DIBFMT_DIB16_RGB;
                dib->funcs = &DIBDRV_funcs_DIB16_RGB;
            }
            else
            {
                init_bit_fields(dib, bit_fields);
                dib->format = DIBFMT_DIB16_BITFIELDS;
                dib->funcs = &DIBDRV_funcs_DIB16_BITFIELDS;
            }
            break;
    
        case 8:
            dib->format = DIBFMT_DIB8;
            dib->funcs = &DIBDRV_funcs_DIB8;
            dib->color_table_size = 256;
            if(bi->biClrUsed) dib->color_table_size = bi->biClrUsed;
            break;
    
        case 4:
            dib->format = DIBFMT_DIB4;
            dib->funcs = &DIBDRV_funcs_DIB4;
            dib->color_table_size = 16;
            if(bi->biClrUsed) dib->color_table_size = bi->biClrUsed;
            break;
    
        case 1:
            dib->format = DIBFMT_DIB1;
            dib->funcs = &DIBDRV_funcs_DIB1;
            dib->color_table_size = 2;
            if(bi->biClrUsed) dib->color_table_size = bi->biClrUsed;
            break;
    
        default:
            dib->format = DIBFMT_UNKNOWN;
            dib->funcs = NULL;
            FIXME("bpp %d not supported\n", dib->bit_count);
            return FALSE;
    }
    TRACE("DIB FORMAT : %s\n", _DIBDRV_dib_format_name(dib));

    /* WARNING : the color table can't be grabbed here, since it's still
       not initialized. It'll be grabbed on RealizeDefaultPalette(),
       which is presumably the first call made after palette initialization.
       So, by now we just set up palette size and leave NULL the palette pointer */

    return TRUE;
}

BOOL _DIBDRV_init_dib_from_BITMAPINFO(DIBDRVBITMAP *dib, BITMAPINFO *bmi)
{
    static const DWORD bit_fields_DIB32_RGB[3] = {0xff0000, 0x00ff00, 0x0000ff};
    static const DWORD bit_fields_DIB16_RGB[3] = {0x7c00, 0x03e0, 0x001f};
    BITMAPINFOHEADER *bi = (BITMAPINFOHEADER *)bmi;
    const DWORD *masks = NULL;
    RGBQUAD *color_table = NULL;
    BYTE *ptr = (BYTE*)bmi + bi->biSize;
    int num_colors = bi->biClrUsed;

    if(bi->biCompression == BI_BITFIELDS)
    {
        masks = (DWORD *)ptr;
        ptr += 3 * sizeof(DWORD);
    }
    else if(bi->biBitCount == 32)
        masks = bit_fields_DIB32_RGB;
    else if(bi->biBitCount == 16)
        masks = bit_fields_DIB16_RGB;

    if(!num_colors && bi->biBitCount <= 8)
        num_colors = 1 << bi->biBitCount;
    if(num_colors)
        color_table = (RGBQUAD*)ptr;
    ptr += num_colors * sizeof(*color_table);

    return _DIBDRV_init_dib(dib, bi, masks, color_table, ptr);
}

void _DIBDRV_copy_dib_color_info(const DIBDRVBITMAP *src, DIBDRVBITMAP *dst)
{
    dst->bit_count        = src->bit_count;
    dst->red_mask         = src->red_mask;
    dst->green_mask       = src->green_mask;
    dst->blue_mask        = src->blue_mask;
    dst->red_len          = src->red_len;
    dst->green_len        = src->green_len;
    dst->blue_len         = src->blue_len;
    dst->red_shift        = src->red_shift;
    dst->green_shift      = src->green_shift;
    dst->blue_shift       = src->blue_shift;
    dst->funcs            = src->funcs;
    dst->color_table_size = src->color_table_size;
    dst->color_table      = NULL;
    if(dst->color_table_size)
    {
        int size = dst->color_table_size * sizeof(dst->color_table[0]);
        dst->color_table = HeapAlloc(GetProcessHeap(), 0, dst->color_table_size * sizeof(dst->color_table[0]));
        memcpy(dst->color_table, src->color_table, size);
    }
}

/* checks whether the format of 2 DIBs are identical
   it checks the pixel bit count and the color table size
   and content, if needed */
BOOL _DIBDRV_dib_formats_match(const DIBDRVBITMAP *d1, const DIBDRVBITMAP *d2)
{
    /* checks at first the format (bit count and color masks) */
    if(d1->format != d2->format)
        return FALSE;

    /* formats matches, now checks color tables if needed */
    switch(d1->format)
    {
        case DIBFMT_DIB32_RGB :
        case DIBFMT_DIB32_BITFIELDS :
        case DIBFMT_DIB24 :
        case DIBFMT_DIB16_RGB :
        case DIBFMT_DIB16_BITFIELDS :
            return TRUE;

        case DIBFMT_DIB1 :
        case DIBFMT_DIB4 :
        /*case DIBFMT_DIB4_RLE :*/
        case DIBFMT_DIB8 :
        /*case DIBFMT_DIB8_RLE :*/
            if(d1->color_table_size != d2->color_table_size)
                return FALSE;
        return !memcmp(d1->color_table, d2->color_table, d1->color_table_size * sizeof(d1->color_table[0]));

    default:
        ERR("Unexpected depth %d\n", d1->bit_count);
        return FALSE;
    }
}

/* convert a given dib into another format.
   We could spilt this into two and have convert_to_8888 and convert_from_8888
   as functions in the primitive function table */
void _DIBDRV_convert_dib(const DIBDRVBITMAP *src, DIBDRVBITMAP *dst)
{
    DWORD color, pixel;
    COLORREF color_ref;
    int x, y;

    TRACE("src:%p, dst:%p\n", src, dst);

    dst->height = src->height;
    dst->width = src->width;
    dst->stride = ((dst->width * dst->bit_count + 31) >> 3) & ~3;
    dst->bits = HeapAlloc(GetProcessHeap(), 0, dst->height * dst->stride);

    if(_DIBDRV_dib_formats_match(src, dst))
    {
        if(src->stride > 0)
            memcpy(dst->bits, src->bits, dst->height * dst->stride);
        else
        {
            BYTE *src_bits = src->bits;
            BYTE *dst_bits = dst->bits;
            for(y = 0; y < dst->height; y++)
            {
                memcpy(dst_bits, src_bits, dst->stride);
                dst_bits += dst->stride;
                src_bits += src->stride;
            }
        }
        return;
    }

    for(y = 0; y < src->height; y++)
    {
        for(x = 0; x < src->width; x++)
        {
            color = src->funcs->get_pixel_rgb(src, x, y);
            color_ref = RGB((color >> 16), (color >> 8), color);
            pixel = dst->funcs->colorref_to_pixel(dst, color_ref);
            dst->funcs->set_pixel(dst, x, y, 0, pixel);
        }
    }
    return;
}

/****************************************************************************
 *       SelectBitmap   (WINEDIB.DRV.@)
 */
HBITMAP DIBDRV_SelectBitmap( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap )
{
    DIBSECTION ds;

    TRACE("physDev:%p, hbitmap : %p\n", physDev, hbitmap);

    if(GetObjectW(hbitmap, sizeof(ds), &ds) == sizeof(BITMAP))
    {
        /* Stock bitmap */
        ds.dsBmih.biWidth = 1;
        ds.dsBmih.biHeight = 1;
        ds.dsBmih.biBitCount = 1;
        ds.dsBmih.biClrUsed = 0;
        ds.dsBm.bmBits = NULL;
    }

    if(physDev->bmp.color_table != NULL)
        HeapFree(GetProcessHeap(), 0, physDev->bmp.color_table);
    physDev->bmp.color_table = NULL;

    _DIBDRV_init_dib(&physDev->bmp, &ds.dsBmih, ds.dsBitfields, NULL, ds.dsBm.bmBits);

    return hbitmap;
}

/****************************************************************************
 *        DIBDRV_CreateBitmap
 */
BOOL DIBDRV_CreateBitmap( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap, LPVOID bmBits )
{
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_DeleteBitmap
 */
BOOL DIBDRV_DeleteBitmap( HBITMAP hbitmap )
{
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetBitmapBits
 */
LONG DIBDRV_GetBitmapBits( HBITMAP hbitmap, void *buffer, LONG count )
{
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return 0;
}

/******************************************************************************
 *             DIBDRV_SetBitmapBits
 */
LONG DIBDRV_SetBitmapBits( HBITMAP hbitmap, const void *bits, LONG count )
{
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return 0;
}
