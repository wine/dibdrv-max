/*
 * DIBDRV implementation of GDI driver graphics functions
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Huw Davies
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

static inline void order_int(int *i1, int *i2)
{
    if(*i1 > *i2)
    {
        int tmp;
        tmp = *i1;
        *i1 = *i2;
        *i2 = tmp;
    }
}

/***********************************************************************
 *           DIBDRV_Arc
 */
BOOL DIBDRV_Arc( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom,
            INT xstart, INT ystart, INT xend, INT yend )
{
    TRACE("physDev:%p, left:%d, top:%d, right:%d, bottom:%d, xstart:%d, ystart:%d, xend:%d, yend:%d\n",
          physDev, left, top, right, bottom, xstart, ystart, xend, yend);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Chord
 */
BOOL DIBDRV_Chord( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom,
              INT xstart, INT ystart, INT xend, INT yend )
{
    TRACE("physDev:%p, left:%d, top:%d, right:%d, bottom:%d, xstart:%d, ystart:%d, xend:%d, yend:%d\n",
          physDev, left, top, right, bottom, xstart, ystart, xend, yend);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Ellipse
 */
BOOL DIBDRV_Ellipse( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom )
{
    TRACE("physDev:%p, left:%d, top:%d, right:%d, bottom:%d\n",
          physDev, left, top, right, bottom);
          
#ifdef DIBDRV_SHOW_STUBS
   FIXME("stub\n");
#endif
   return TRUE;
}

/**********************************************************************
 *          DIBDRV_ExtFloodFill
 */
BOOL DIBDRV_ExtFloodFill( DIBDRVPHYSDEV *physDev, INT x, INT y, COLORREF color,
                     UINT fillType )
{
    TRACE("physDev:%p, x:%d, y:%d, color:%x, fillType:%d\n",
          physDev, x, y, color, fillType);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetDCOrgEx
 */
BOOL DIBDRV_GetDCOrgEx( DIBDRVPHYSDEV *physDev, LPPOINT lpp )
{
    TRACE("physDev:%p, lpp:%p\n", physDev, lpp);

#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetPixel
 */
COLORREF DIBDRV_GetPixel( DIBDRVPHYSDEV *physDev, INT x, INT y )
{
    DWORD c;

    TRACE("physDev:%p, x:%d, y:%d\n", physDev, x, y);
          
    c = physDev->bmp.funcs->get_pixel_rgb(&physDev->bmp, x, y);
    return c;
}

/***********************************************************************
 *           DIBDRV_LineTo
 */
BOOL DIBDRV_LineTo( DIBDRVPHYSDEV *physDev, INT x, INT y )
{
    POINT cur_pos;

    TRACE("physDev:%p, x:%d, y:%d\n", physDev, x, y);
          
    GetCurrentPositionEx(physDev->hdc, &cur_pos);

    _DIBDRV_reset_dash_origin(physDev);

    if(cur_pos.y == y) physDev->pen_hline(physDev, cur_pos.x, x, y);
    else if(cur_pos.x == x) physDev->pen_vline(physDev, x, cur_pos.y, y);
    else physDev->pen_line(physDev, cur_pos.x, cur_pos.y, x, y);

    return TRUE;
}

/***********************************************************************
 *           DIBDRV_PaintRgn
 */
BOOL DIBDRV_PaintRgn( DIBDRVPHYSDEV *physDev, HRGN hrgn )
{
    TRACE("physDev:%p, hrgn:%p\n", physDev, hrgn);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Pie
 */
BOOL DIBDRV_Pie( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom,
            INT xstart, INT ystart, INT xend, INT yend )
{
    TRACE("physDev:%p, left:%d, top:%d, right:%d, bottom:%d, xstart:%d, ystart:%d, xend:%d, yend:%d\n",
          physDev, left, top, right, bottom, xstart, ystart, xend, yend);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_Polygon
 */
BOOL DIBDRV_Polygon( DIBDRVPHYSDEV *physDev, const POINT* pt, INT count )
{
    TRACE("physDev:%p, pt:%p, count:%d\n", physDev, pt, count);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_Polyline
 */
BOOL DIBDRV_Polyline( DIBDRVPHYSDEV *physDev, const POINT* pt, INT count )
{
    TRACE("physDev:%p, pt:%p, count:%d\n", physDev, pt, count);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_PolyPolygon
 */
BOOL DIBDRV_PolyPolygon( DIBDRVPHYSDEV *physDev, const POINT* pt, const INT* counts, UINT polygons)
{
    TRACE("physDev:%p, pt:%p, counts:%p, polygons:%d\n", physDev, pt, counts, polygons);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_PolyPolyline
 */
BOOL DIBDRV_PolyPolyline( DIBDRVPHYSDEV *physDev, const POINT* pt, const DWORD* counts,
                     DWORD polylines )
{
    TRACE("physDev:%p, pt:%p, counts:%p, polylines:%d\n", physDev, pt, counts, polylines);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Rectangle
 */
BOOL DIBDRV_Rectangle( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom)
{
    int i;

    TRACE("physDev:%p, left:%d, top:%d, right:%d, bottom:%d\n",
          physDev, left, top, right, bottom);
          
    order_int(&left, &right);
    order_int(&top, &bottom);

    _DIBDRV_reset_dash_origin(physDev);
    /* Draw the perimeter starting at the top-right corner and move anti-clockwise */
    physDev->pen_hline(physDev, right - 1, left - 1, top);
    physDev->pen_vline(physDev, left, top + 1, bottom);
    physDev->pen_hline(physDev, left + 1, right, bottom - 1);
    physDev->pen_vline(physDev, right - 1, bottom - 2, top);

    for (i = top + 1; i < bottom - 1; i++)
        physDev->brush_hline(physDev, left + 1, right - 1, i);

    return TRUE;
}

/***********************************************************************
 *           DIBDRV_RoundRect
 */
BOOL DIBDRV_RoundRect( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right,
                  INT bottom, INT ell_width, INT ell_height )
{
    TRACE("physDev:%p, left:%d, top:%d, right:%d, bottom:%d, ell_width:%d, ell_height:%d\n",
          physDev, left, top, right, bottom, ell_width, ell_height);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_SetPixel
 */
COLORREF DIBDRV_SetPixel( DIBDRVPHYSDEV *physDev, INT x, INT y, COLORREF color )
{
    DWORD and, xor, c;
    
    TRACE("physDev:%p, x:%d, y:%d, color:%x\n", physDev, x, y, color);
          
    /* gets previous pixel */
    c = physDev->bmp.funcs->get_pixel_rgb(&physDev->bmp, x, y);
 
    /* calculates AND and XOR from color */
    _DIBDRV_calc_and_xor_masks(GetROP2(physDev->hdc), color, &and, &xor);
    
    /* sets the pixel */
    physDev->bmp.funcs->set_pixel(&physDev->bmp, x, y, and, xor);

    /* returns previous pixel color */
    return c;
}

/***********************************************************************
 *           DIBDRV_SetDCOrg
 */
DWORD DIBDRV_SetDCOrg( DIBDRVPHYSDEV *physDev, INT x, INT y )
{
    TRACE("physDev:%p, x:%d, y:%d\n", physDev, x, y);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return 0;
}
