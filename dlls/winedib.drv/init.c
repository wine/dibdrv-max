/*
 * DIB driver initialization functions
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/* some screen caps */
unsigned int screen_width;
unsigned int screen_height;
unsigned int screen_bpp;
unsigned int screen_depth;
RECT virtual_screen_rect;

/* a few dynamic device caps */
static int log_pixels_x;  /* pixels per logical inch in x direction */
static int log_pixels_y;  /* pixels per logical inch in y direction */
static int horz_size;     /* horz. size of screen in millimeters */
static int vert_size;     /* vert. size of screen in millimeters */
static int palette_size;
static int device_init_done;

/* NOTE :
    Removing TC_RA_ABLE avoids bitmapped fonts, so FT_Face is always non-NULL
    Adding TC_VA_ABLE forces to use gdi fonts always, so we can get an FT_Face
*/
unsigned int text_caps = (TC_OP_CHARACTER | TC_OP_STROKE | TC_CP_STROKE |
                          TC_CR_ANY | TC_SA_DOUBLE | TC_SA_INTEGER |
                          TC_SA_CONTIN | TC_UA_ABLE | TC_SO_ABLE /* | TC_RA_ABLE */ | TC_VA_ABLE);
                          /* X11R6 adds TC_SF_X_YINDEP, Xrender adds TC_VA_ABLE */


static const WCHAR dpi_key_name[] = {'S','o','f','t','w','a','r','e','\\','F','o','n','t','s','\0'};
static const WCHAR dpi_value_name[] = {'L','o','g','P','i','x','e','l','s','\0'};

/******************************************************************************
 *      get_dpi
 *
 * get the dpi from the registry
 */
static DWORD get_dpi( void )
{
    DWORD dpi = 96;
    HKEY hkey;

    if (RegOpenKeyW(HKEY_CURRENT_CONFIG, dpi_key_name, &hkey) == ERROR_SUCCESS)
    {
        DWORD type, size, new_dpi;

        size = sizeof(new_dpi);
        if(RegQueryValueExW(hkey, dpi_value_name, NULL, &type, (void *)&new_dpi, &size) == ERROR_SUCCESS)
        {
            if(type == REG_DWORD && new_dpi != 0)
                dpi = new_dpi;
        }
        RegCloseKey(hkey);
    }
    return dpi;
}

/**********************************************************************
 *       device_init
 *
 * Perform initializations needed upon creation of the first device.
 */
static void device_init(void)
{
    Display *display;
    Screen *screen;

    /* opens default X11 Display */
    if( (display = XOpenDisplay(NULL)) == NULL)
        return;

    /* gets default screen */
    screen = XDefaultScreenOfDisplay(display);

    /* gets screen sizes */
    screen_width = XWidthOfScreen(screen);
    screen_height = XHeightOfScreen(screen);

    /* not sure about these ones... */
    screen_bpp = XDefaultDepthOfScreen(screen);
    screen_depth = XPlanesOfScreen(screen);
    virtual_screen_rect.left = 0;
    virtual_screen_rect.top = 0;
    virtual_screen_rect.right = screen_width;
    virtual_screen_rect.bottom = screen_height;

    /* dummy ? */
    palette_size = 0;

    /* Initialize device caps */
    log_pixels_x = log_pixels_y = get_dpi();
    horz_size = MulDiv( screen_width, 254, log_pixels_x * 10 );
    vert_size = MulDiv( screen_height, 254, log_pixels_y * 10 );

    device_init_done = TRUE;
}

static inline void clear_dib_info(DIBDRVBITMAP *dib)
{
    dib->bits = dib->color_table = NULL;
}

/**********************************************************************
 *           DIBDRV_CreateDC
 */
BOOL DIBDRV_CreateDC( HDC hdc, DIBDRVPHYSDEV **pdev, LPCWSTR driver, LPCWSTR device,
                      LPCWSTR output, const DEVMODEW* initData )
{
    DIBDRVPHYSDEV *physDev;

    TRACE("hdc:%p, pdev:%p, driver:%s, device:%s, output:%s, initData:%p\n",
          hdc, pdev, debugstr_w(driver), debugstr_w(device), debugstr_w(output), initData);

    if (!device_init_done)
        device_init();
    
    physDev = HeapAlloc( GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(DIBDRVPHYSDEV) );
    if (!physDev)
        return FALSE;
    
    *pdev = physDev;
    physDev->hdc = hdc;

    clear_dib_info(&physDev->bmp);
    clear_dib_info(&physDev->brush_dib);
    physDev->brush_and_bits = NULL;
    physDev->brush_xor_bits = NULL;

    /* FIXME : sets freetype font to null; checks wether it is
       correct or if we must initialize to some default font */
    physDev->face = 0;
    
    /* initializes text color and table */
    physDev->textColor = -1;
    DIBDRV_SetTextColor(physDev, 0);

    return TRUE;
}

/**********************************************************************
 *           DIBDRV_DeleteDC
 */
BOOL DIBDRV_DeleteDC( DIBDRVPHYSDEV *physDev )
{
    TRACE("physDev:%p\n", physDev);
    
    if (physDev->bmp.color_table)
    {
        HeapFree( GetProcessHeap(), 0, physDev->bmp.color_table );
        physDev->bmp.color_table = NULL;
    }
    HeapFree( GetProcessHeap(), 0, physDev );
    return TRUE;
}

/**********************************************************************
 *           DIBDRV_ExtEscape
 */
INT DIBDRV_ExtEscape( DIBDRVPHYSDEV *physDev, INT escape, INT in_count, LPCVOID in_data,
                      INT out_count, LPVOID out_data )
{
    TRACE("physDev:%p, escape:%d, in_count:%d, in_data:%p, out_count:%d, out_data:%p\n",
          physDev, escape, in_count, in_data, out_count, out_data);
          
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return 0;
}

/***********************************************************************
 *           DIBDRV_GetDeviceCaps
 */
INT DIBDRV_GetDeviceCaps( DIBDRVPHYSDEV *physDev, INT cap )
{
    switch(cap)
    {
    case DRIVERVERSION:
        return 0x300;
    case TECHNOLOGY:
        return DT_RASDISPLAY;
    case HORZSIZE:
        return horz_size;
    case VERTSIZE:
        return vert_size;
    case HORZRES:
        return screen_width;
    case VERTRES:
        return screen_height;
    case DESKTOPHORZRES:
        return virtual_screen_rect.right - virtual_screen_rect.left;
    case DESKTOPVERTRES:
        return virtual_screen_rect.bottom - virtual_screen_rect.top;
    case BITSPIXEL:
        return screen_bpp;
    case PLANES:
        return 1;
    case NUMBRUSHES:
        return -1;
    case NUMPENS:
        return -1;
    case NUMMARKERS:
        return 0;
    case NUMFONTS:
        return 0;
    case NUMCOLORS:
        /* MSDN: Number of entries in the device's color table, if the device has
         * a color depth of no more than 8 bits per pixel.For devices with greater
         * color depths, -1 is returned. */
        return (screen_depth > 8) ? -1 : (1 << screen_depth);
    case CURVECAPS:
        return (CC_CIRCLES | CC_PIE | CC_CHORD | CC_ELLIPSES | CC_WIDE |
                CC_STYLED | CC_WIDESTYLED | CC_INTERIORS | CC_ROUNDRECT);
    case LINECAPS:
        return (LC_POLYLINE | LC_MARKER | LC_POLYMARKER | LC_WIDE |
                LC_STYLED | LC_WIDESTYLED | LC_INTERIORS);
    case POLYGONALCAPS:
        return (PC_POLYGON | PC_RECTANGLE | PC_WINDPOLYGON | PC_SCANLINE |
                PC_WIDE | PC_STYLED | PC_WIDESTYLED | PC_INTERIORS);
    case TEXTCAPS:
        return text_caps;
    case CLIPCAPS:
        return CP_REGION;
    case RASTERCAPS:
        return (RC_BITBLT | RC_BANDING | RC_SCALING | RC_BITMAP64 | RC_DI_BITMAP |
                RC_DIBTODEV | RC_BIGFONT | RC_STRETCHBLT | RC_STRETCHDIB | RC_DEVBITS |
                (palette_size ? RC_PALETTE : 0));
    case SHADEBLENDCAPS:
        return (SB_GRAD_RECT | SB_GRAD_TRI | SB_CONST_ALPHA | SB_PIXEL_ALPHA);
    case ASPECTX:
    case ASPECTY:
        return 36;
    case ASPECTXY:
        return 51;
    case LOGPIXELSX:
        return log_pixels_x;
    case LOGPIXELSY:
        return log_pixels_y;
    case CAPS1:
        FIXME("(%p): CAPS1 is unimplemented, will return 0\n", physDev->hdc );
        /* please see wingdi.h for the possible bit-flag values that need
           to be returned. */
        return 0;
    case SIZEPALETTE:
        return palette_size;
    case NUMRESERVED:
    case COLORRES:
    case PHYSICALWIDTH:
    case PHYSICALHEIGHT:
    case PHYSICALOFFSETX:
    case PHYSICALOFFSETY:
    case SCALINGFACTORX:
    case SCALINGFACTORY:
    case VREFRESH:
    case BLTALIGNMENT:
        return 0;
    default:
        FIXME("(%p): unsupported capability %d, will return 0\n", physDev->hdc, cap );
        return 0;
    }
}
