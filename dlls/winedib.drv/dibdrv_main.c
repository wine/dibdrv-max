/*
 * DIBDRV initialization code
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/***********************************************************************
 *           DIBDRV initialization routine
 */
BOOL WINAPI DllMain( HINSTANCE hinst, DWORD reason, LPVOID reserved )
{
    BOOL ret = TRUE;

    switch(reason)
    {
    case DLL_PROCESS_ATTACH:
        /* do attach */

#ifdef HAVE_FREETYPE
        /* initializes freetype library */
        if(!_DIBDRV_FreeType_Init())
        {
            ERR("Couldn't initialize freetype library.\n");
        }

#endif /* HAVE_FREETYPE */
        break;
    case DLL_THREAD_DETACH:
        /* do thread detach */
        break;
    case DLL_PROCESS_DETACH:
        /* do detach */

#ifdef HAVE_FREETYPE
        /* terminates freetype library */
        _DIBDRV_FreeType_Terminate();
#endif /* HAVE_FREETYPE */
        break;
    }
    return ret;
}
