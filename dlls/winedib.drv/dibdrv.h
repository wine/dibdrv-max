/*
 * DIB driver private definitions
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Huw Davies
 * Copyright 2008 Massimo Del Fedele
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WINE_DIBDRV_H
#define __WINE_DIBDRV_H

#include <stdarg.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "windef.h"
#include "winbase.h"
#include "winerror.h"
#include "wingdi.h"
#include "wine/list.h"
#include "wine/library.h"
#include "wine/debug.h"
#include "wingdi.h"
#include "winreg.h"
#include "wine/winbase16.h" /* GlobalLock16 */

#include "freetype.h"

/* Enable this if we want trace unimplemented stubs
   That's done because logs becomes big and painting
   slow if all FIXMEs for stubs are shown
*/
/* #define DIBDRV_SHOW_STUBS */

/******************************************************************************************/
/*                                     DIB PRIMITIVES                                     */
/******************************************************************************************/

/* BASIC ROP HELPER
 * Decompose the 16 ROP2s into an expression of the form
 *
 * D = (D & A) ^ X
 *
 * Where A and X depend only on P (and so can be precomputed). */
void _DIBDRV_calc_and_xor_masks(INT rop, DWORD color, DWORD *and, DWORD *xor);

struct _DIBDRVBITMAP;
typedef struct _DIBDRV_PRIMITIVE_FUNCS
{
    void  (* solid_hline)       (const struct _DIBDRVBITMAP *dib, int start, int end, int row, DWORD and, DWORD xor);
    void  (* pattern_hline)     (const struct _DIBDRVBITMAP *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset);
    void  (* solid_vline)       (const struct _DIBDRVBITMAP *dib, int col, int start, int end, DWORD and, DWORD xor);
    void* (* get_pixel_ptr)     (const struct _DIBDRVBITMAP *dib, int x, int y);
    void  (* set_pixel)         (const struct _DIBDRVBITMAP *dib, int x, int y, DWORD and, DWORD xor);
    DWORD (* get_pixel_rgb)     (const struct _DIBDRVBITMAP *dib, int x, int y);
    DWORD (* colorref_to_pixel) (const struct _DIBDRVBITMAP *dib, COLORREF color);
    void  (* get_line)          (const struct _DIBDRVBITMAP *dib, INT line, INT startx, int width, void *buf);
    void  (* put_line)          (const struct _DIBDRVBITMAP *dib, INT line, INT startx, int width, void *buf);
    void  (* bitblt)            (const struct _DIBDRVBITMAP *dstBmp, INT xDst, INT yDst, INT width, INT height,
                                 const struct _DIBDRVBITMAP *srcBmp, INT xSrc, INT ySrc, DWORD rop );
    void  (* freetype_blit)     (const struct _DIBDRVBITMAP *dib, int x, int y, FT_Bitmap *bmp);

} DIBDRV_PRIMITIVE_FUNCS;

extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB32_RGB;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB32_BITFIELDS;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB24;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB16_RGB;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB16_BITFIELDS;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB8;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB4;
extern DIBDRV_PRIMITIVE_FUNCS DIBDRV_funcs_DIB1;

/******************************************************************************************/

typedef enum _DIBFORMAT
{
    DIBFMT_UNKNOWN          =   0,
    DIBFMT_DIB1             =   1,
    DIBFMT_DIB4             =   2,
    DIBFMT_DIB4_RLE         =   3,
    DIBFMT_DIB8             =   4,
    DIBFMT_DIB8_RLE         =   5,
    DIBFMT_DIB16_RGB        =   6,
    DIBFMT_DIB16_BITFIELDS  =   7,
    DIBFMT_DIB24            =   8,
    DIBFMT_DIB32_RGB        =   9,
    DIBFMT_DIB32_BITFIELDS  =  10
} DIBFORMAT;

/* DIB driver's generic bitmap structure */
typedef struct _DIBDRVBITMAP
{
    /* bitmap format of dib */
    DIBFORMAT format;

    /* pointer to top left corner of bitmap */
    void *bits;

    /* bitmap dimensions */
    INT width;
    INT height;

    /* bitmap stride (== width in bytes) of a bitmap line */
    /* negative for a bottom-up bitmap */
    INT stride;

    /* size in bits of a pixel */
    INT size;
    
    /* number of bits in bitmap */
    INT bit_count;

    /* calculated numbers for bitfields */
    DWORD red_mask, green_mask, blue_mask;
    int red_shift, green_shift, blue_shift; /* shifting required within a COLORREF's BYTE */
    int red_len, green_len, blue_len;       /* size of the field */

    /* color table and its size */
    RGBQUAD *color_table;
    DWORD color_table_size;

    /* text color table for antialiased fonts */
    COLORREF textColorTable[256];

    /* current drawing address */
    void *addr;
    INT shift;

    /* primitive function pointers */
    DIBDRV_PRIMITIVE_FUNCS *funcs;

} DIBDRVBITMAP;

/* dash patterns */
typedef struct
{
    DWORD count;
    DWORD dashes[6];
} dash_pattern_t;

/* DIB driver physical device */
typedef struct _DIBDRVPHYSDEV
{
    /* device context */
    HDC hdc;

    /* windows bitmap handle */
    HBITMAP hbitmap;

    /* dib bitmap info */
    DIBDRVBITMAP bmp;

    /* pen */
    COLORREF pen_colorref;
    DWORD pen_color, pen_and, pen_xor;
    const dash_pattern_t *pen_pattern;
    DWORD cur_dash, left_in_dash;
    enum mark_space { mark, space } mark_space;

    /* brush */
    COLORREF brush_colorref;
    DWORD brush_color, brush_and, brush_xor;
    DIBDRVBITMAP brush_dib;
    void *brush_and_bits, *brush_xor_bits;

    /* bkgnd */
    DWORD bkgnd_color, bkgnd_and, bkgnd_xor;

    /* pen drawing functions */
    void (* pen_hline)(struct _DIBDRVPHYSDEV *pdev, int start, int end, int row);
    void (* pen_vline)(struct _DIBDRVPHYSDEV *pdev, int col, int start, int end);
    void (* pen_line)(struct _DIBDRVPHYSDEV *pdev, int x1, int y1, int x2, int y2);
    void (* brush_hline)(struct _DIBDRVPHYSDEV *pdev, int start, int end, int row);
    
    /* text color */
    COLORREF textColor;

    /* freetype face associated to current DC HFONT */
    FT_Face face;

} DIBDRVPHYSDEV;

BOOL _DIBDRV_init_dib(DIBDRVBITMAP *dib, const BITMAPINFOHEADER *bi, const DWORD *bit_fields,
                      const RGBQUAD *color_table, void *bits);
BOOL _DIBDRV_init_dib_from_BITMAPINFO(DIBDRVBITMAP *dib, BITMAPINFO *bmi);
void _DIBDRV_copy_dib_color_info(const DIBDRVBITMAP *src, DIBDRVBITMAP *dst);
void _DIBDRV_convert_dib(const DIBDRVBITMAP *src, DIBDRVBITMAP *dst);
void _DIBDRV_reset_dash_origin(DIBDRVPHYSDEV *physDev);
INT DIBDRV_GetDeviceCaps( DIBDRVPHYSDEV *physDev, INT cap );
COLORREF DIBDRV_SetTextColor( DIBDRVPHYSDEV *physDev, COLORREF color );

/* gets human-readable dib format name */
const char *_DIBDRV_dib_format_name(DIBDRVBITMAP const *bmp);

/* checks whether the format of 2 DIBs are identical
   it checks the pixel bit count and the color table size
   and content, if needed */
BOOL _DIBDRV_dib_formats_match(const DIBDRVBITMAP *d1, const DIBDRVBITMAP *d2);

/* the ROP3 operations
   this is a BIG case block; beware that some
   commons ROP3 operations will be optimized
   from inside blt routines
*/
DWORD DIBDRV_ROP3(DWORD p, DWORD s, DWORD d, BYTE rop);

#endif /* __WINE_DIBDRV_H */
