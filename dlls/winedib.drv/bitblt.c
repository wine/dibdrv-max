/*
 * DIBDRV bit-blit operations
 *
 * Copyright 2007 Jesse Allen
 * Copyright 2008 Huw Davies
 * Copyright 2008 Massimo Del Fedele
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include "wine/port.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/***********************************************************************
 *           get_rop2_from_rop
 *
 * Returns the binary rop that is equivalent to the provided ternary rop
 * if the src bits are ignored.
 */
static inline DWORD get_rop2_from_rop(DWORD rop)
{
    return (((rop >> 18) & 0x0c) | ((rop >> 16) & 0x03)) + 1;
}

static inline BOOL rop_uses_src(DWORD rop)
{
    return (((rop & 0xcc0000) >> 2) != (rop & 0x330000));
}

/***********************************************************************
 *           DIBDRV_AlphaBlend
 */
BOOL DIBDRV_AlphaBlend( DIBDRVPHYSDEV *devDst, INT xDst, INT yDst, INT widthDst, INT heightDst,
                        DIBDRVPHYSDEV *devSrc, INT xSrc, INT ySrc, INT widthSrc, INT heightSrc,
                        BLENDFUNCTION blendfn)
{
    TRACE("devDst:%p, xDst:%d, yDst:%d, widthDst:%d, heightDst:%d, devSrc:%p, xSrc:%d, ySrc:%d, widthSrc:%d, heightSrc:%d, blendfn:%p\n",
          devDst, xDst, yDst, widthDst, heightDst, devSrc, xSrc, ySrc, widthSrc, heightSrc, blendfn);
#ifdef DIBDRV_SHOW_STUBS
    FIXME("stub\n");
#endif
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_BitBlt
 */
BOOL DIBDRV_BitBlt( DIBDRVPHYSDEV *physDevDst, INT xDst, INT yDst,
                    INT width, INT height, DIBDRVPHYSDEV *physDevSrc,
                    INT xSrc, INT ySrc, DWORD rop )
{
    DIBDRVBITMAP *srcBmp, *dstBmp;

    /* gets both source and dest bitmaps */
    srcBmp = & physDevSrc->bmp;
    dstBmp = & physDevDst->bmp;
    
    TRACE("physDevDst:%p(%s), xDst:%d, yDst:%d, width:%d, height:%d, physDevSrc:%p(%s), xSrc:%d, ySrc:%d, rop:%06x\n",
                    physDevDst, _DIBDRV_dib_format_name(dstBmp), xDst, yDst, width, height,
                    physDevSrc, _DIBDRV_dib_format_name(srcBmp), xSrc, ySrc, rop);
          
    /* corrects sizes match bitmaps ones */
    /* FIXME : must be changed for clipping.... */
    if(xSrc >= srcBmp->width || ySrc >= srcBmp->height)
    {
        FIXME("Bad xSrc or ySrc\n");
        return FALSE;
    }
    if(xSrc < 0)
    {
        width += xSrc;
        if(width <= 0)
        {
            FIXME("xSrc\n");
            return FALSE;
        }
        xDst -= xSrc;
        xSrc = 0;
    }
    if(ySrc < 0)
    {
        height += ySrc;
        if(height <= 0)
        {
            FIXME("ySrc\n");
            return FALSE;
        }
        yDst -= ySrc;
        ySrc = 0;
    }
    if(xDst >= dstBmp->width || yDst >= dstBmp->height)
    {
        FIXME("bitmap don't fit on dest one\n");
        return FALSE;
    }
    if(xSrc + width > srcBmp->width)
        width = srcBmp->width - xSrc;
    if(ySrc + height > srcBmp->height)
        height = srcBmp->height - ySrc;
    if(xDst + width > dstBmp->width)
        width = dstBmp->width - xDst;
    if(yDst + height > dstBmp->height)
        height = dstBmp->height - yDst;
    
    dstBmp->funcs->bitblt(dstBmp, xDst, yDst, width, height, srcBmp, xSrc, ySrc, rop);
    
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_StretchBlt
 */
BOOL DIBDRV_StretchBlt( DIBDRVPHYSDEV *physDevDst, INT xDst, INT yDst,
                        INT widthDst, INT heightDst,
                        DIBDRVPHYSDEV *physDevSrc, INT xSrc, INT ySrc,
                        INT widthSrc, INT heightSrc, DWORD rop )
{
    FIXME("TEMPORARY - JUST BitBlt\n");
    return DIBDRV_BitBlt(physDevDst, xDst, yDst, widthDst, heightDst, physDevSrc, xSrc, ySrc, rop);
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_PatBlt
 */
BOOL DIBDRV_PatBlt( DIBDRVPHYSDEV *physDev, INT left, INT top, INT width, INT height, DWORD rop )
{
    INT i;
    DWORD old_rop2;
    
    TRACE("physDev:%p, left:%d, top:%d, width:%d, height:%d, rop:%06x\n", physDev, left, top, width, height, rop);

    if(rop_uses_src(rop)) return FALSE;

    old_rop2 = SetROP2(physDev->hdc, get_rop2_from_rop(rop));

    for (i = 0; i < height; i++)
        physDev->brush_hline(physDev, left, left + width, top + i);

    SetROP2(physDev->hdc, old_rop2);

    return TRUE;
}
