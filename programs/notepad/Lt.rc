/*
 *  Notepad (Lithuanian resources)
 *
 *  Copyright 2009 Aurimas Fišeras <aurimas@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/* UTF-8 */
#pragma code_page(65001)

LANGUAGE LANG_LITHUANIAN, SUBLANG_NEUTRAL

MAIN_MENU MENU
{
 POPUP "&Failas" {
  MENUITEM "&Naujas\tCtrl+N",            CMD_NEW
  MENUITEM "&Atverti...\tCtrl+O",        CMD_OPEN
  MENUITEM "&Išsaugoti\tCtrl+S",         CMD_SAVE
  MENUITEM "Išsaugoti k&aip...",         CMD_SAVE_AS
  MENUITEM SEPARATOR
  MENUITEM "S&pausdinti...\tCtrl+P",     CMD_PRINT
  MENUITEM "Puslapio &nuostatos...",     CMD_PAGE_SETUP
  MENUITEM "Spaus&dintuvo nuostatos...", CMD_PRINTER_SETUP
  MENUITEM SEPARATOR
  MENUITEM "Iš&eiti",                  CMD_EXIT
 }
POPUP "&Keisti" {
  MENUITEM "&Atšaukti\tCtrl+Z",       CMD_UNDO
  MENUITEM SEPARATOR
  MENUITEM "&Iškirpti\tCtrl+X",       CMD_CUT
  MENUITEM "&Kopijuoti\tCtrl+C",      CMD_COPY
  MENUITEM "Į&dėti\tCtrl+V",          CMD_PASTE
  MENUITEM "Iš&valyti\tDel",          CMD_DELETE
  MENUITEM SEPARATOR
  MENUITEM "&Pažymėti viską\tCtrl+A", CMD_SELECT_ALL
  MENUITEM "&Laikas/Data\tF5",        CMD_TIME_DATE
  MENUITEM SEPARATOR
  MENUITEM "Laužyti ilgas &eilutes",  CMD_WRAP
  MENUITEM "Š&riftas...",             CMD_FONT
 }
POPUP "&Paieška" {
  MENUITEM "&Rasti...\tCtrl+F", CMD_SEARCH
  MENUITEM "Rasti k&itą\tF3",   CMD_SEARCH_NEXT
 }
POPUP "&Žinynas" {
  MENUITEM "&Turinys",             CMD_HELP_CONTENTS
  MENUITEM "&Paieška...",          CMD_HELP_SEARCH
  MENUITEM "Pa&galba apie žinyną", CMD_HELP_ON_HELP
  MENUITEM SEPARATOR
  MENUITEM "&Apie Notepad",        CMD_HELP_ABOUT_NOTEPAD
 }
}

/* Dialog `Page setup' */

DIALOG_PAGESETUP DIALOG 0, 0, 225, 95
STYLE DS_MODALFRAME | WS_CAPTION | WS_SYSMENU
FONT 8, "MS Shell Dlg"
CAPTION "Puslapio nuostatos"
{
LTEXT    "A&ntraštė:",  0x140,  10, 07, 40, 15
EDITTEXT IDC_PAGESETUP_HEADERVALUE,   60, 05,110, 12, WS_BORDER | WS_TABSTOP
LTEXT    "&Poraštė:",   0x142,   10, 24, 40, 15
EDITTEXT IDC_PAGESETUP_FOOTERVALUE,   60, 22,110, 12, WS_BORDER | WS_TABSTOP

GROUPBOX "Paraštės (milimetrais):",      0x144,     10, 43,160, 45
LTEXT    "&Kairėje:",        0x145,   20, 55, 30, 10, WS_CHILD
EDITTEXT IDC_PAGESETUP_LEFTVALUE,     50, 55, 35, 11, WS_CHILD | WS_BORDER | WS_TABSTOP
LTEXT    "&Viršutinė:",      0x148,    20, 73, 30, 10, WS_CHILD
EDITTEXT IDC_PAGESETUP_TOPVALUE,      50, 73, 35, 11, WS_CHILD | WS_BORDER | WS_TABSTOP
LTEXT    "&Dešinėje:",       0x14B, 100, 55, 30, 10, WS_CHILD
EDITTEXT IDC_PAGESETUP_RIGHTVALUE,   130, 55, 35, 11, WS_CHILD | WS_BORDER | WS_TABSTOP
LTEXT    "&Apatinė:",        0x14E, 100, 73, 30, 10, WS_CHILD
EDITTEXT IDC_PAGESETUP_BOTTOMVALUE,  130, 73, 35, 11, WS_CHILD | WS_BORDER | WS_TABSTOP

DEFPUSHBUTTON "Gerai",         IDOK,                180,  3, 40, 15, WS_TABSTOP
PUSHBUTTON    "Atsisakyti",    IDCANCEL,            180, 21, 40, 15, WS_TABSTOP
PUSHBUTTON    "&Žinynas",      IDHELP,              180, 39, 40, 15, WS_TABSTOP
}

STRINGTABLE DISCARDABLE
{
STRING_PAGESETUP_HEADERVALUE,   "&f"
STRING_PAGESETUP_FOOTERVALUE,   "Puslapis &p"

STRING_NOTEPAD,                        "Notepad"
STRING_ERROR,                          "KLAIDA"
STRING_WARNING,                        "ĮSPĖJIMAS"
STRING_INFO,                           "Informacija"

STRING_UNTITLED,                       "Be pavadinimo"

STRING_ALL_FILES,                      "Visi failai (*.*)"
STRING_TEXT_FILES_TXT,                 "Tekstiniai failai (*.txt)"

STRING_TOOLARGE,                       "Failas '%s' yra per didelis notepad programai.\n\
Prašome naudoti kitą tekstų rengyklę."
STRING_NOTEXT,                         "Jūs neįvedėte jokio teksto.\n\
Prašome ką nors įvesti ir mėginti dar kartą"
STRING_DOESNOTEXIST,                   "Failas '%s'\nneegzistuoja\n\n\
Ar norite sukurti naują failą?"
STRING_NOTSAVED,                       "Failas '%s'\nbuvo pakeistas\n\n\
Ar norite išsaugoti pakeitimus?"
STRING_NOTFOUND,                       "Frazė '%s' nerasta."
STRING_OUT_OF_MEMORY,                  "Neužtenka atminties operacijai užbaigti.\
\nNorėdami atlaisvinti atminties uždarykite vieną ar daugiau programų."

}
#pragma code_page(default)
